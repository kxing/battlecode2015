package sprint_submission;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class HQ extends Structure {
    
    public HQ(RobotController rc) {
        super(rc);
    }
    
    public void performUnitSpecificActions() throws GameActionException {
        switch (Clock.getRoundNum()) {
            case 0:
                Direction directionToEnemy = myHQLocation.directionTo(enemyHQLocation);
                this.trySpawn(RobotType.BEAVER, directionToEnemy);
                break;
            case 1:
                break;
            default:
                updateBeaverGoal();
                break;
        }
        
        updateMinerGoal();
        
        SensorInfo sensorInfo = this.sense();
        this.attack(sensorInfo);
        
        this.transferSupplies(sensorInfo);
        
        if (Clock.getRoundNum() < 400 && this.com.getStructureGoal(RobotType.MINERFACTORY) == null) {
            this.com.setStructureGoal(RobotType.MINERFACTORY, RobotType.MINER);
        }
        if (this.rc.getTeamOre() >= 2 * RobotType.DRONE.oreCost && this.com.getStructureGoal(RobotType.HELIPAD) != RobotType.DRONE) {
            this.com.setStructureGoal(RobotType.HELIPAD, RobotType.DRONE);
        }
        
//        if (this.rc.getTeamOre() >= 2 * RobotType.LAUNCHER.oreCost && this.com.getStructureGoal(RobotType.AEROSPACELAB) != RobotType.LAUNCHER) {
//            this.com.setStructureGoal(RobotType.AEROSPACELAB, RobotType.LAUNCHER);
//        }
        if (this.rc.getTeamOre() >= 2 * RobotType.TANK.oreCost && this.com.getStructureGoal(RobotType.TANKFACTORY) != RobotType.TANK) {
            this.com.setStructureGoal(RobotType.TANKFACTORY, RobotType.TANK);
        }
        
        MapLocation enemyTarget = this.getEnemyTarget();
        if (!enemyTarget.equals(this.com.getAttackTarget())) {
            this.com.setAttackTarget(enemyTarget);
        }
    }
    
    private void transferSupplies(SensorInfo si) throws GameActionException {
        final int SUPPLY_MINIMUM = 10000;
        for (RobotInfo ri: si.myInfos) {
            if (ri.type == RobotType.TANK &&
                    ri.supplyLevel <= SUPPLY_MINIMUM &&
                    myLocation.distanceSquaredTo(ri.location) <= GameConstants.SUPPLY_TRANSFER_RADIUS_SQUARED) {
                double toTransfer = Math.min(SUPPLY_MINIMUM, this.rc.getSupplyLevel() / 2);
                this.rc.transferSupplies((int)(toTransfer), ri.location);
            }
        }
    }
    
    private MapLocation getStructureLocation() throws GameActionException {
        int x = myHQLocation.x;
        int y = myHQLocation.y;
        
        for (int radius = 3; radius >= 1; radius--) {
            // Try looking for squares within the radius.
            for (int dx = -radius; dx <= radius; dx++) {
                for (int offset : new int[]{radius, -radius}) {
                    MapLocation candidateLocation = new MapLocation(x + dx, y + offset);
                    if (isGoodBuildLocation(candidateLocation)) {
                        return candidateLocation;
                    }
                }
            }
            for (int dy = -radius; dy <= radius; dy++) {
                for (int offset : new int[]{radius, -radius}) {
                    MapLocation candidateLocation = new MapLocation(x + offset, y + dy);
                    if (isGoodBuildLocation(candidateLocation)) {
                        return candidateLocation;
                    }
                }
            }
        }
        return null;
    }
    
    private boolean isGoodBuildLocation(MapLocation ml) throws GameActionException {
        if (this.getStructureAt(ml) != null) {
            // Something else is already built here.
            return false;
        }
        // A building location is good, if it doesn't disconnect reachable squares, and if its
        // adjacent structures still have somewhere to build.
        
        boolean[] blocked = new boolean[ALL_DIRECTIONS.length];
        for (int i = 0; i < ALL_DIRECTIONS.length; i++) {
            Direction d = ALL_DIRECTIONS[i];
            MapLocation adjacentLocation = ml.add(d);
            if (this.rc.senseTerrainTile(adjacentLocation) != TerrainTile.NORMAL) {
                blocked[i] = true;
            }
        }
        
        boolean currentlyBlocked = blocked[0];
        int lastTF = -1;
        int lastFT = -1;
        for (int i = 1; i < blocked.length; i++) {
            if (currentlyBlocked && !blocked[i]) {
                if (lastTF != -1) {
                    return false;
                }
                lastTF = i;
            } else if (!currentlyBlocked && blocked[i]) {
                if (lastFT != -1) {
                    return false;
                }
                lastFT = i;
            }
        }
        
        // Check that adjacent structures have somewhere to spawn.
        MapLocation[] adjacentStructures = new MapLocation[ALL_DIRECTIONS.length];
        int numAdjacentStructures = 0;
        for (Direction d : ALL_DIRECTIONS) {
            MapLocation adjacentLocation = ml.add(d);
            if (this.getStructureAt(adjacentLocation) != null) {
                adjacentStructures[numAdjacentStructures++] = adjacentLocation;
            }
        }
        
        for (int i = 0; i < numAdjacentStructures; i++) {
            MapLocation adjacentlocation = adjacentStructures[i];
            boolean canSpawn = false;
            for (Direction d: ALL_DIRECTIONS) {
                boolean spawnableSquare = (this.rc.senseTerrainTile(adjacentlocation.add(d)) == TerrainTile.NORMAL);
                boolean unoccupied = (this.getStructureAt(adjacentlocation) == null);
                if (spawnableSquare && unoccupied) {
                    canSpawn = true;
                    break;
                }
            }
            if (!canSpawn) {
                return false;
            }
        }
        
        return true;
    }
    
    private boolean isStructureBuilt(RobotType type) throws GameActionException {
        MapLocation ml = this.com.getProposedStructureLocation(type);
        if (ml == null) {
            return false;
        }
        return (this.getStructureAt(ml) == type);
    }
    
    private void updateBeaverGoal() throws GameActionException {
        int desiredBeaverGoal = Com.BEAVER_GOAL_GO_MINING;
        int currentBeaverGoal = this.com.getBeaverGoal();
        
        // TODO(kxing): Make this better.
        RobotType structureToBuild = null;
        if (!this.isStructureBuilt(RobotType.MINERFACTORY)) {
            desiredBeaverGoal = Com.BEAVER_GOAL_BUILD_MINER_FACTORY;
            structureToBuild = RobotType.MINERFACTORY;
        } else if (!this.isStructureBuilt(RobotType.HELIPAD)) {
            desiredBeaverGoal = Com.BEAVER_GOAL_BUILD_HELIPAD;
            structureToBuild = RobotType.HELIPAD;
        } else if (!this.isStructureBuilt(RobotType.BARRACKS)) {
            desiredBeaverGoal = Com.BEAVER_GOAL_BUILD_BARRACKS;
            structureToBuild = RobotType.BARRACKS;
        } else if (!this.isStructureBuilt(RobotType.TANKFACTORY)) {
            desiredBeaverGoal = Com.BEAVER_GOAL_BUILD_TANK_FACTORY;
            structureToBuild = RobotType.TANKFACTORY;
        }
//        else if (!this.isStructureBuilt(RobotType.AEROSPACELAB)) {
//            desiredBeaverGoal = Com.BEAVER_GOAL_BUILD_AEROSPACE_LAB;
//            structureToBuild = RobotType.AEROSPACELAB;
//        }
        
        if (desiredBeaverGoal != currentBeaverGoal) {
            this.com.setBeaverGoal(desiredBeaverGoal);
            if (structureToBuild != null) {
                this.com.setProposedStructureLocation(structureToBuild, this.getStructureLocation());
            }
        }
    }
    
    private void updateMinerGoal() throws GameActionException {
        Direction directionToEnemy = myHQLocation.directionTo(enemyHQLocation);
        Direction adjustedDirection = directionToEnemy.rotateLeft();
        
        // TODO(kxing): Make this much better.
        MapLocation minerGoal = this.myHQLocation.add(adjustedDirection, GameConstants.MAP_MIN_HEIGHT);
        if (!minerGoal.equals(this.com.getMinerGoal())) {
            this.com.setMinerGoal(minerGoal);
        }
    }
    
    private MapLocation getEnemyTarget() {
        MapLocation[] enemyTowers = this.rc.senseEnemyTowerLocations();
        MapLocation furthestTower = enemyHQLocation;
        int furthestDistance = 0;
        
        for (MapLocation ml: enemyTowers) {
            int distance = ml.distanceSquaredTo(enemyHQLocation);
            if (furthestDistance < distance) {
                furthestDistance = distance;
                furthestTower = ml;
            }
        }
        
        return furthestTower;
    }
}
