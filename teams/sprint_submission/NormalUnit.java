package sprint_submission;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public abstract class NormalUnit extends Bot {
    
    protected final Nav nav;
    protected final DirectionMap dm;
    
    public NormalUnit(RobotController rc) {
        super(rc);
        
        this.nav = new Nav(rc);
        this.dm = new DirectionMap();
    }
    
    public abstract void performUnitSpecificActions() throws GameActionException;
    
    public void takeTurn() throws GameActionException {
        // Update location, as necessary.
        this.myLocation = rc.getLocation();
        
        SensorInfo sensorInfo = this.sense();
        if (this.isDamaging(myType)) {
            this.attack(sensorInfo);
        }
        
        this.performUnitSpecificActions();
        
        if (this.rc.isCoreReady()) {
            this.dm.clear();

            Direction movementDirection = this.getCombatMovementDirection(sensorInfo);
            if (movementDirection == null) {
                movementDirection = this.getNonCombatMovementDirection(sensorInfo, this.dm);
            }
            
            if (movementDirection != null && movementDirection != Direction.NONE) {
                this.rc.move(movementDirection);
            }
        }
    }
    
    // The core is guaranteed to be ready if this is called.
    protected abstract Direction getNonCombatMovementDirection(SensorInfo sensorInfo, DirectionMap dm) throws GameActionException;
    
    // Returns the result of taking n steps from start to end.
    private static MapLocation stepsToward(MapLocation start, MapLocation end, int n) {
        MapLocation current = start;
        for (int i = 0; i < n; i++) {
            Direction d = current.directionTo(end);
            current = current.add(d);
        }
        return current;
    }
    
    private Direction getCombatMovementDirection(SensorInfo sensorInfo) throws GameActionException {
        for (Direction d : ALL_DIRECTIONS) {
            if (d != Direction.NONE && !this.rc.canMove(d)) {
                this.dm.addWeight(d, DirectionMap.ILLEGAL_MOVE);
            }
        }
        
        // Don't get too close to the enemy HQ.
        final int SPLASH_DAMAGE_TOWERS = 5;
        int extraStepsToEnemyHQ = 0;
        
        if (this.rc.senseEnemyTowerLocations().length >= SPLASH_DAMAGE_TOWERS) {
            extraStepsToEnemyHQ = 1;
        }
        if (NormalUnit.stepsToward(myLocation, enemyHQLocation, 1 + extraStepsToEnemyHQ).distanceSquaredTo(enemyHQLocation) <= RobotType.HQ.attackRadiusSquared) {
            boolean shouldRespectHQ = true;
            if (enemyHQLocation.equals(this.com.getAttackTarget()) &&
                    Clock.getRoundNum() <= this.com.getIgnoreAttackDamage() + Bot.IGNORE_DAMAGE_ROUNDS) {
                // We're told to ignore the tower.
                shouldRespectHQ = false;
            }
            if (shouldRespectHQ) {
                for (Direction d : ALL_DIRECTIONS) {
                    MapLocation candidateLocation = myLocation.add(d);
                    int distanceToEnemyHQ = NormalUnit.stepsToward(candidateLocation, enemyHQLocation, extraStepsToEnemyHQ).distanceSquaredTo(enemyHQLocation);
                    if (distanceToEnemyHQ <= RobotType.HQ.attackRadiusSquared) {
                        this.dm.addWeight(d, -RobotType.HQ.attackPower / RobotType.HQ.attackDelay / distanceToEnemyHQ);
                    }
                }
            }
        }
        
        // Don't get too close to the enemy tower.
        MapLocation[] enemyTowers = this.rc.senseEnemyTowerLocations();
        for (MapLocation enemyTower: enemyTowers) {
            Direction directionToEnemyTower = myLocation.directionTo(enemyTower);
            if (myLocation.add(directionToEnemyTower).distanceSquaredTo(enemyTower) > RobotType.TOWER.attackRadiusSquared) {
                // Tower doesn't really matter here.
                continue;
            }
            if (enemyTower.equals(this.com.getAttackTarget()) &&
                    Clock.getRoundNum() <= this.com.getIgnoreAttackDamage() + Bot.IGNORE_DAMAGE_ROUNDS) {
                // We're told to ignore the tower.
                continue;
            }
            for (Direction d : ALL_DIRECTIONS) {
                MapLocation candidateLocation = myLocation.add(d);
            
                int distanceToEnemyTower = candidateLocation.distanceSquaredTo(enemyTower);
                if (distanceToEnemyTower <= RobotType.TOWER.attackRadiusSquared) {
                    this.dm.addWeight(d, -RobotType.TOWER.attackPower / RobotType.TOWER.attackDelay / distanceToEnemyTower);
                }
            }
        }
        
        RobotInfo[] myInfos = sensorInfo.myInfos;
        RobotInfo[] enemyInfos = sensorInfo.enemyInfos;
        
        for (RobotInfo enemyInfo : enemyInfos) {
            MapLocation enemyLocation = enemyInfo.location;
            RobotType enemyType = enemyInfo.type;
            
            // TODO(kxing): Gravitate towards towers if they are already attacking something.
            if (enemyType == RobotType.HQ || enemyType == RobotType.TOWER) {
                // Ignore, because we've already considered towers and HQ.
                continue;
            }
            
            boolean isDangerous = false;
            if (this.isDamaging(enemyInfo.type)) {
                isDangerous = true;
                for (RobotInfo ri: sensorInfo.myInfos) {
                    if (ri.location.distanceSquaredTo(enemyLocation) <= enemyType.attackRadiusSquared) {
                        isDangerous = true;
                        break;
                    }
                }
            }
            
            for (Direction d : ALL_DIRECTIONS) {
                MapLocation candidateLocation = this.myLocation.add(d);
                int distance = candidateLocation.distanceSquaredTo(enemyLocation);
                if (isDangerous && distance <= enemyType.attackRadiusSquared) {
                    this.dm.addWeight(d, -enemyType.attackPower / enemyType.attackDelay);
                } else {
                    this.dm.addWeight(d, 1.0 / (1.0 + distance));
                }
            }
        }
        
        if (this.dm.isMoveForced()) {
            Direction forcedDirection = this.dm.getBestDirection();
            if (forcedDirection != Direction.NONE) {
                if (!rc.canMove(forcedDirection)) {
                    System.out.println("??? " + forcedDirection.toString());
                }
                return forcedDirection;
            }
            return Direction.NONE;
        }
        return null;
    }
    
    protected boolean isDamaging(RobotType type) {
        switch (type) {
            case AEROSPACELAB:
            case BARRACKS:
            case COMPUTER:
            case HANDWASHSTATION:
            case HELIPAD:
            case MINERFACTORY:
            case SUPPLYDEPOT:
            case TANKFACTORY:
            case TECHNOLOGYINSTITUTE:
            case TRAININGFIELD:
                return false;
            case BASHER:
            case BEAVER:
            case COMMANDER:
            case DRONE:
            case HQ:
            case LAUNCHER:
            case MINER:
            case MISSILE:
            case SOLDIER:
            case TANK:
            case TOWER:
                return true;
            default:
                throw new AssertionError("Robot type not recognized: " + type.toString());
        }
    }
}
