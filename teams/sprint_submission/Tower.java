package sprint_submission;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Tower extends Structure {
    
    public Tower(RobotController rc) {
        super(rc);
    }

    public void performUnitSpecificActions() throws GameActionException {
        SensorInfo sensorInfo = this.sense();
        this.attack(sensorInfo);
    }
}
