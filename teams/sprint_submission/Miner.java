package sprint_submission;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Miner extends NormalUnit {
    
    private static final double ORE_THRESHOLD = 12.0;
    private static final double LAZY_MINING_THRESHOLD = 8.0;
    
    public Miner(RobotController rc) {
        super(rc);
    }

    public void performUnitSpecificActions() throws GameActionException {
    }
    
    protected Direction getNonCombatMovementDirection(SensorInfo sensorInfo, DirectionMap dm) throws GameActionException {
        if (this.rc.senseOre(this.myLocation) >= Miner.LAZY_MINING_THRESHOLD) {
            this.rc.mine();
            return Direction.NONE;
        }
        
        MapLocation localMiningGoal = this.getLocalMiningGoal();
        
        if (localMiningGoal != null) {
            this.nav.setGoal(localMiningGoal);
            return this.nav.getMovementDirection(dm);
        }
        
        MapLocation minerGoal = this.com.getMinerGoal();
        if (minerGoal == null) {
            minerGoal = this.myHQLocation;
        }
        
        this.nav.setGoal(minerGoal);
        return this.nav.getMovementDirection(dm);
    }
    
    private MapLocation getLocalMiningGoal() throws GameActionException {
        MapLocation currentGoal = this.nav.getCurrentGoal();
        if (currentGoal != null && this.rc.senseOre(currentGoal) >= Miner.ORE_THRESHOLD) {
            return currentGoal;
        }
        
        MapLocation closestLocation = null;
        int bestDistance = Integer.MAX_VALUE;
        
        for (int dx = -3; dx <= 3; dx++) {
            for (int dy = -3; dy <= 3; dy++) {
                MapLocation ml = this.myLocation.add(dx, dy);
                if (this.rc.senseOre(ml) >= Miner.ORE_THRESHOLD &&
                        this.getStructureAt(ml) == null) {
                    int distance = dx * dx + dy * dy;
                    if (bestDistance > distance) {
                        bestDistance = distance;
                        closestLocation = ml;
                    }
                }
            }
        }
        return closestLocation;
    }
}
