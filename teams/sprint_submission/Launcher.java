package sprint_submission;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Launcher extends NormalUnit {

    public Launcher(RobotController rc) {
        super(rc);
    }

    public void performUnitSpecificActions() throws GameActionException {
        MapLocation enemyTarget = this.com.getAttackTarget();
        if (enemyTarget == null) {
            enemyTarget = enemyHQLocation;
        }
        
        if (this.shouldSpawnMissiles(enemyTarget)) {
            Direction directionToTarget = myLocation.directionTo(enemyTarget);
            if (this.rc.canLaunch(directionToTarget)) {
                this.rc.launchMissile(directionToTarget);
            }
        }
    }

    protected Direction getNonCombatMovementDirection(SensorInfo sensorInfo, DirectionMap dm) throws GameActionException {
        MapLocation enemyTarget = this.com.getAttackTarget();
        if (enemyTarget == null) {
            enemyTarget = enemyHQLocation;
        }
        
        if (this.shouldStayPut(enemyTarget)) {
            // Don't move if we can help it, while sieging.
            return Direction.NONE;
        }
        
        this.nav.setGoal(enemyTarget);
        return this.nav.getMovementDirection(this.dm);
    }
    
    private boolean shouldStayPut(MapLocation enemyTarget) {
        Direction missileDirection = myLocation.directionTo(enemyTarget);
        if (myLocation.distanceSquaredTo(enemyTarget) > RobotType.TOWER.attackRadiusSquared) {
            MapLocation missileSpawnLocation= myLocation.add(missileDirection);
            if (missileSpawnLocation.distanceSquaredTo(enemyTarget) <= RobotType.TOWER.attackRadiusSquared) {
                // Don't move if we can help it, while sieging.
                MapLocation[] enemyTowers = this.rc.senseEnemyTowerLocations();
                for (MapLocation ml: enemyTowers) {
                    if (ml.equals(enemyTarget)) {
                        continue;
                    }
                    if (missileSpawnLocation.distanceSquaredTo(ml) <= RobotType.TOWER.attackRadiusSquared) {
                        // Too close to another tower that is still up.
                        return false;
                    }
                }
                
                if (!enemyTarget.equals(enemyHQLocation) &&
                        missileSpawnLocation.distanceSquaredTo(enemyHQLocation) <= RobotType.HQ.attackRadiusSquared) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }
    
    private static final int SEIGE_PERIOD = GameConstants.MISSILE_MAX_COUNT * GameConstants.MISSILE_SPAWN_FREQUENCY;
    
    private boolean shouldSpawnMissiles(MapLocation attackTarget) {
        if (this.rc.getMissileCount() == 0 || attackTarget == null) {
            return false;
        }
        
        // Sync up attacking - hold missiles for later.
        if (Clock.getRoundNum() % SEIGE_PERIOD > GameConstants.MISSILE_MAX_COUNT + 1) {
            return false;
        }
        
        MapLocation ml = myLocation;
        for (int i = 0; i <= GameConstants.MISSILE_LIFESPAN; i++) {
            Direction directionToTarget = ml.directionTo(attackTarget);
            ml = ml.add(directionToTarget);
        }
        
        return ml.equals(attackTarget);
    }
    
}
