package sprint_submission;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Com {
    private final RobotController rc;
    private final MapLocation myHQLocation;
    // The smallest possible X and Y values on the map, given only the location of the HQ.
    private final int minX;
    private final int minY;
    
    // Proposed locations: channels 0-6, inclusive.
    private static final int PROPOSED_LOCATION_START = 0;
    
    // Map dimension information: channels 7-10, inclusive.
    private static final int MIN_X_CHANNEL = 7;
    private static final int MAX_X_CHANNEL = 8;
    private static final int MIN_Y_CHANNEL = 9;
    private static final int MAX_Y_CHANNEL = 10;
    
    // Commands for beavers and miners: channels 11-12, inclusive.
    private static final int BEAVER_CHANNEL = 11;
    private static final int MINER_CHANNEL = 12;
    
    // Commands for static structures: channels 13-19, inclusive.
    private static final int MINER_FACTORY_CHANNEL = 13;
    private static final int TECHNOLOGY_INSTITUTE_CHANNEL = 14;
    private static final int TRAINING_FIELD_CHANNEL = 15;
    private static final int BARRACKS_CHANNEL = 16;
    private static final int TANK_FACTORY_CHANNEL = 17;
    private static final int HELIPAD_CHANNEL = 18;
    private static final int AEROSPACE_LABL_CHANNEL = 19;
    
    // Commands for attacking: channel 20-21.
    private static final int ATTACK_CHANNEL = 20;
    private static final int IGNORE_ATTACK_DAMAGE_CHANNEL = 21;
    
    // Map information: channels 8415 - 65535.
    private static final int MAP_CHANNEL_START =
            GameConstants.BROADCAST_MAX_CHANNELS -
            (2 * GameConstants.MAP_MAX_HEIGHT - 1) * (2 * GameConstants.MAP_MAX_WIDTH - 1) + 1;
            
    
    public Com(RobotController rc) {
        this.rc = rc;
        this.myHQLocation = rc.senseHQLocation();
        this.minX = this.myHQLocation.x - GameConstants.MAP_MAX_WIDTH + 1;
        this.minY = this.myHQLocation.y - GameConstants.MAP_MAX_HEIGHT + 1;
    }
    
    // -------------------------------------------------------------------------
    // For encoding where to build all of the structures.
    // -------------------------------------------------------------------------
    
    public void setProposedStructureLocation(RobotType type, MapLocation ml) throws GameActionException {
        int channelData = this.mapLocationToInt(ml);
        rc.broadcast(PROPOSED_LOCATION_START + Com.structureTypeToInt(type), channelData);
    }
    
    public MapLocation getProposedStructureLocation(RobotType type) throws GameActionException {
        int channelData = rc.readBroadcast(PROPOSED_LOCATION_START + Com.structureTypeToInt(type));
        return this.intToMapLocation(channelData);
    }
    
    // -------------------------------------------------------------------------
    // For encoding the map dimensions.
    // -------------------------------------------------------------------------
    
    private static final int MAP_EXTREMA_OFFSET = Math.max(GameConstants.MAP_MAX_HEIGHT, GameConstants.MAP_MAX_WIDTH);
    
    private void debug_mapDimension(int channel) throws GameActionException {
        if (this.rc.readBroadcast(channel) == 0) {
            throw new AssertionError("map dimension not set yet");
        }
    }
    
    public void setMinX(int minX) throws GameActionException {
        this.rc.broadcast(MIN_X_CHANNEL, MAP_EXTREMA_OFFSET + minX - this.myHQLocation.x);
    }
    
    public int getMinX() throws GameActionException {
        debug_mapDimension(MIN_X_CHANNEL);
        return this.rc.readBroadcast(MIN_X_CHANNEL) + this.myHQLocation.x - MAP_EXTREMA_OFFSET;
    }
    
    public boolean isMinXSet() throws GameActionException {
        return (this.rc.readBroadcast(MIN_X_CHANNEL) == 0);
    }
    
    public void setMinY(int minY) throws GameActionException {
        this.rc.broadcast(MIN_Y_CHANNEL, MAP_EXTREMA_OFFSET + minY - this.myHQLocation.y);
    }
    
    public int getMinY() throws GameActionException {
        debug_mapDimension(MIN_Y_CHANNEL);
        return this.rc.readBroadcast(MIN_Y_CHANNEL) + this.myHQLocation.y - MAP_EXTREMA_OFFSET;
    }
    
    public boolean isMinYSet() throws GameActionException {
        return (this.rc.readBroadcast(MIN_Y_CHANNEL) == 0);
    }
    
    public void setMaxX(int maxX) throws GameActionException {
        this.rc.broadcast(MAX_X_CHANNEL, MAP_EXTREMA_OFFSET + maxX - this.myHQLocation.x);
    }
    
    public int getMaxX() throws GameActionException {
        return this.rc.readBroadcast(MAX_X_CHANNEL) + this.myHQLocation.x - MAP_EXTREMA_OFFSET;
    }
    
    public boolean isMaxXSet() throws GameActionException {
        return (this.rc.readBroadcast(MAX_X_CHANNEL) == 0);
    }
    
    public void setMaxY(int maxY) throws GameActionException {
        this.rc.broadcast(MAX_Y_CHANNEL, MAP_EXTREMA_OFFSET + maxY - this.myHQLocation.y);
    }
    
    public int getMaxY() throws GameActionException {
        return this.rc.readBroadcast(MAX_Y_CHANNEL) + this.myHQLocation.y - MAP_EXTREMA_OFFSET;
    }
    
    public boolean isMaxYSet() throws GameActionException {
        return (this.rc.readBroadcast(MAX_Y_CHANNEL) == 0);
    }
    
    // -------------------------------------------------------------------------
    // For encoding what the beaver and miners should be doing.
    // -------------------------------------------------------------------------
    
    public static final int BEAVER_GOAL_GO_MINING = 0;
    public static final int BEAVER_GOAL_BUILD_MINER_FACTORY = 1;
    public static final int BEAVER_GOAL_BUILD_TECHNOLOGY_INSTITUTE = 2;
    public static final int BEAVER_GOAL_BUILD_TRAINING_FIELD = 3;
    public static final int BEAVER_GOAL_BUILD_BARRACKS = 4;
    public static final int BEAVER_GOAL_BUILD_TANK_FACTORY = 5;
    public static final int BEAVER_GOAL_BUILD_HELIPAD = 6;
    public static final int BEAVER_GOAL_BUILD_AEROSPACE_LAB = 7;
    
    public int getBeaverGoal() throws GameActionException {
        return this.rc.readBroadcast(BEAVER_CHANNEL);
    }
    
    public void setBeaverGoal(int beaverGoal) throws GameActionException {
        this.rc.broadcast(BEAVER_CHANNEL, beaverGoal);
    }
    
    public MapLocation getMinerGoal() throws GameActionException {
        int channelData = this.rc.readBroadcast(MINER_CHANNEL);
        return this.intToMapLocation(channelData);
    }
    
    public void setMinerGoal(MapLocation ml) throws GameActionException {
        int channelData = this.mapLocationToInt(ml);
        this.rc.broadcast(MINER_CHANNEL, channelData);
    }
    
    // -------------------------------------------------------------------------
    // For what each of the structures should be spawning.
    // -------------------------------------------------------------------------
    
    public void setStructureGoal(RobotType structureType, RobotType unitType) throws GameActionException {
        this.rc.broadcast(Com.structureTypeToChannel(structureType), Com.unitTypeToInt(unitType));
    }
    
    public RobotType getStructureGoal(RobotType structureType) throws GameActionException {
        int channelData = this.rc.readBroadcast(Com.structureTypeToChannel(structureType));
        return Com.intToUnitType(channelData);
    }
    
    // -------------------------------------------------------------------------
    // For keeping track of attacking.
    // -------------------------------------------------------------------------
    public void setAttackTarget(MapLocation ml) throws GameActionException {
        this.rc.broadcast(ATTACK_CHANNEL, this.mapLocationToInt(ml));
    }
    
    public MapLocation getAttackTarget() throws GameActionException {
        int channelData = this.rc.readBroadcast(ATTACK_CHANNEL);
        return this.intToMapLocation(channelData);
    }
    
    public void setIgnoreAttackDamage(int ignoreRound) throws GameActionException {
        this.rc.broadcast(IGNORE_ATTACK_DAMAGE_CHANNEL, ignoreRound);
    }
    
    public int getIgnoreAttackDamage() throws GameActionException {
        return this.rc.readBroadcast(IGNORE_ATTACK_DAMAGE_CHANNEL);
    }
    
    // -------------------------------------------------------------------------
    // For encoding the actual map, along with the ore counts.
    // -------------------------------------------------------------------------
    
    // The default value (0) always corresponds to null, which will be convenient
    // because the message array should be initialized to 0 anyways.
    private static final int NO_VALUE = 0;
    
    // Constants for encoding the map state.
    private static final int OFF_MAP_INT = 1;
    private static final int VOID_INT = 2;
    private static final int NORMAL_OFFSET = 3;
    
    public TerrainTile getTerrainTile(MapLocation ml) throws GameActionException {
        int channelData = this.rc.readBroadcast(this.mapChannel(ml));
        return Com.intToTerrainTile(channelData);
    }
    
    private void debug_setTerrainTile(MapLocation ml, TerrainTile tile) throws GameActionException {
        if (tile == TerrainTile.NORMAL) {
            throw new AssertionError("Should have called setOreAmount() instead of setTerrainTile()");
        }
        TerrainTile currentTile = this.getTerrainTile(ml);
        if (currentTile != TerrainTile.UNKNOWN && currentTile != tile) {
            throw new AssertionError("Logged terrain tiles don't match for " + ml.toString());
        }
    }
    
    public void setTerrainTile(MapLocation ml, TerrainTile tile) throws GameActionException {
        debug_setTerrainTile(ml, tile);
        this.rc.broadcast(this.mapChannel(ml), Com.terrainTileToInt(tile));
    }
    
    private void debug_setOreAmount(MapLocation ml) throws GameActionException {
        if (this.getTerrainTile(ml) != TerrainTile.NORMAL) {
            throw new AssertionError("Called setOreAmount() on a tile with no ore.");
        }
    }
    
    public void setOreAmount(MapLocation ml, double amount) throws GameActionException {
        debug_setOreAmount(ml);
        int channelData = NORMAL_OFFSET + (int)(amount);
        this.rc.broadcast(this.mapChannel(ml), channelData);
    }
    
    private void debug_getOreAmount(MapLocation ml) throws GameActionException {
        if (this.getTerrainTile(ml) != TerrainTile.NORMAL) {
            throw new AssertionError("Called getOreAmount() on a tile with no ore.");
        }
    }
    
    public int getOreAmount(MapLocation ml) throws GameActionException {
        debug_getOreAmount(ml);
        int channelData = this.rc.readBroadcast(this.mapChannel(ml));
        return channelData - NORMAL_OFFSET;
    }
    
    // -------------------------------------------------------------------------
    // Here is where the actual encoding is done.
    // -------------------------------------------------------------------------
    
    // For encoding/decoding MapLocations
    private static final int X_BASE = 2 * GameConstants.MAP_MAX_HEIGHT;
    
    private int mapLocationToInt(MapLocation ml) {
        if (ml == null) {
            return Com.NO_VALUE;
        }
        return (ml.x - this.minX) * Com.X_BASE + (ml.y - this.minY) + 1;
    }
    
    private MapLocation intToMapLocation(int encodedInt) {
        if (encodedInt == Com.NO_VALUE) {
            return null;
        }
        int x = (encodedInt - 1) / Com.X_BASE + this.minX;
        int y = (encodedInt - 1) % Com.X_BASE + this.minY;
        
        return new MapLocation(x, y);
    }
    
    // For encoding/decoding TerrainTiles
    private static int terrainTileToInt(TerrainTile tile) {
        switch (tile) {
            case UNKNOWN:
                return NO_VALUE;
            case OFF_MAP:
                return OFF_MAP_INT;
            case VOID:
                return VOID_INT;
            default:
                return NORMAL_OFFSET;
        }
    }
    
    private static TerrainTile intToTerrainTile(int encodedInt) {
        switch (encodedInt) {
            case NO_VALUE:
                return TerrainTile.UNKNOWN;
            case OFF_MAP_INT:
                return TerrainTile.OFF_MAP;
            case VOID_INT:
                return TerrainTile.VOID;
            default:
                return TerrainTile.NORMAL;
        }
    }
    
    // For computing offsets, etc.
    
    private static int structureTypeToInt(RobotType type) {
        switch (type) {
            case MINERFACTORY:
                return 0;
            case TECHNOLOGYINSTITUTE:
                return 1;
            case TRAININGFIELD:
                return 2;
            case BARRACKS:
                return 3;
            case TANKFACTORY:
                return 4;
            case HELIPAD:
                return 5;
            case AEROSPACELAB:
                return 6;
            default:
                throw new AssertionError("Not a unit-building structure that needs to be built.");
        }
    }
    
    // For converting map locations to channel numbers.
    private int mapChannel(MapLocation ml) {
        int offsetX = ml.x - this.minX;
        int offsetY = ml.y - this.minY;
        
        return MAP_CHANNEL_START + offsetX * (2 * GameConstants.MAP_MAX_HEIGHT - 1) + offsetY;
    }
    
    // For converting beaver goals to robot types to build.
    public static RobotType beaverGoalToRobotType(int beaverGoal) {
        switch (beaverGoal) {
            case 1:
                return RobotType.MINERFACTORY;
            case 2:
                return RobotType.TECHNOLOGYINSTITUTE;
            case 3:
                return RobotType.TRAININGFIELD;
            case 4:
                return RobotType.BARRACKS;
            case 5:
                return RobotType.TANKFACTORY;
            case 6:
                return RobotType.HELIPAD;
            case 7:
                return RobotType.AEROSPACELAB;
            default:
                throw new AssertionError("beaver goal unexpected: " + beaverGoal);
        }
    }
    
    // For converting between unit types to build and ints for encoding.
    private static final RobotType[] UNIT_TYPES = new RobotType[] {
        null,
        RobotType.MINER,
        RobotType.COMPUTER,
        RobotType.SOLDIER,
        RobotType.BASHER,
        RobotType.DRONE,
        RobotType.TANK,
        RobotType.COMMANDER,
        RobotType.LAUNCHER,
    };
    
    public static int unitTypeToInt(RobotType type) {
        if (type == null) {
            return 0;
        }
        switch (type) {
            case MINER:
                return 1;
            case COMPUTER:
                return 2;
            case SOLDIER:
                return 3;
            case BASHER:
                return 4;
            case DRONE:
                return 5;
            case TANK:
                return 6;
            case COMMANDER:
                return 7;
            case LAUNCHER:
                return 8;
            default:
                throw new AssertionError("Unit type not spawnable from a structure: " + type.toString());
        }
    }
    
    private static RobotType intToUnitType(int i) {
        return Com.UNIT_TYPES[i];
    }
    
    // For telling structures what to build.
    
    private static int structureTypeToChannel(RobotType type) {
        switch (type) {
            case MINERFACTORY:
                return MINER_FACTORY_CHANNEL;
            case TECHNOLOGYINSTITUTE:
                return TECHNOLOGY_INSTITUTE_CHANNEL;
            case TRAININGFIELD:
                return TRAINING_FIELD_CHANNEL;
            case BARRACKS:
                return BARRACKS_CHANNEL;
            case TANKFACTORY:
                return TANK_FACTORY_CHANNEL;
            case HELIPAD:
                return HELIPAD_CHANNEL;
            case AEROSPACELAB:
                return AEROSPACE_LABL_CHANNEL;
            default:
                throw new AssertionError("Structure type not recognized: " + type.toString());
        }
    }
}
