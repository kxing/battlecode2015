package sprint_submission;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public abstract class Bot {
    
    protected final RobotController rc;
    protected final RobotType myType;
    protected final Team myTeam;
    protected final Team enemyTeam;
    protected final MapLocation myHQLocation;
    protected final MapLocation enemyHQLocation;
    protected final Com com;
    
    protected MapLocation myLocation;
    
    public static final Direction[] ALL_DIRECTIONS = new Direction[] {
        Direction.NORTH,
        Direction.NORTH_EAST,
        Direction.EAST,
        Direction.SOUTH_EAST,
        Direction.SOUTH,
        Direction.SOUTH_WEST,
        Direction.WEST,
        Direction.NORTH_WEST,
    };
    
    // One spot to build every unit-building structures.
    public static final RobotType[] UNIT_SPAWNING_STRUCTURES = new RobotType[] {
        RobotType.MINERFACTORY,
        RobotType.TECHNOLOGYINSTITUTE,
        RobotType.TRAININGFIELD,
        RobotType.BARRACKS,
        RobotType.TANKFACTORY,
        RobotType.HELIPAD,
        RobotType.AEROSPACELAB,
    };
    
    public static final int IGNORE_DAMAGE_ROUNDS = 20;
    
    public Bot(RobotController rc) {
        this.rc = rc;
        this.myType = rc.getType();
        this.myTeam = rc.getTeam();
        this.enemyTeam = rc.getTeam().opponent();
        this.myHQLocation = rc.senseHQLocation();
        this.enemyHQLocation = rc.senseEnemyHQLocation();
        this.com = new Com(rc);
        
        this.myLocation = rc.getLocation();
    }
    
    public void run() {
        int lastRoundNum = Clock.getRoundNum();
        while (true) {
            try {
                this.takeTurn();
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            int currentRoundNum = Clock.getRoundNum();
            if (currentRoundNum > lastRoundNum + 1) {
                System.err.println("Exceeded bytecode limit");
            }
            lastRoundNum = currentRoundNum;
            
            this.rc.yield();
        }
    }
    
    public abstract void takeTurn() throws GameActionException;
    
    public SensorInfo sense() throws GameActionException {
        RobotInfo[] myInfos = this.rc.senseNearbyRobots(myType.sensorRadiusSquared, myTeam);
        RobotInfo[] enemyInfos = this.rc.senseNearbyRobots(myType.sensorRadiusSquared, enemyTeam);
        
        return new SensorInfo(myInfos, enemyInfos);
    }
    
    public boolean trySpawn(RobotType type, Direction direction) throws GameActionException {
        if (!this.rc.isCoreReady() || this.rc.getTeamOre() < type.oreCost) {
            return false;
        }
        
        Direction[] directions = new Direction[]{
            direction,
            direction.rotateLeft(),
            direction.rotateRight(),
            direction.rotateLeft().rotateLeft(),
            direction.rotateRight().rotateRight(),
            direction.opposite().rotateRight(),
            direction.opposite().rotateLeft(),
            direction.opposite(),
        };
        
        for (Direction d : directions) {
            if (this.rc.canSpawn(d, type)) {
                this.rc.spawn(d, type);
                return true;
            }
        }
        
        return false;
    };
    
    public boolean attack(SensorInfo sensorInfo) throws GameActionException {
        if (!rc.isWeaponReady()) {
            return false;
        }
        MapLocation attackTarget = this.com.getAttackTarget();
        if (attackTarget != null && myLocation.distanceSquaredTo(attackTarget) <= myType.attackRadiusSquared) {
            // Attack the tower/HQ if you can.
            this.rc.attackLocation(attackTarget);
            return true;
        }
        
        // Attack closest enemy for now.
        // TODO(kxing): Make this a lot smarter.
        MapLocation closestLocation = null;
        int closestDistance = Integer.MAX_VALUE;
        
        for (RobotInfo ri : sensorInfo.enemyInfos) {
            int distance = myLocation.distanceSquaredTo(ri.location);
            if (distance > myType.attackRadiusSquared) {
                continue;
            }
            
            boolean isBetter = false;
            if (closestLocation == null) {
                isBetter = true;
            } else if (distance < closestDistance) {
                isBetter = true;
            }
            
            if (isBetter) {
                closestLocation = ri.location;
                closestDistance = distance;
            }
        }
        
        if (closestLocation != null) {
            rc.attackLocation(closestLocation);
            return true;
        }
        return false;
    }
    
    public static boolean isUnitSpawningStructure(RobotType type) {
        switch (type) {
            case AEROSPACELAB:
            case BARRACKS:
            case HELIPAD:
            case MINERFACTORY:
            case TANKFACTORY:
            case TECHNOLOGYINSTITUTE:
            case TRAININGFIELD:
                return true;
            default:
                return false;
        }
    }
        
    public RobotType getStructureAt(MapLocation ml) throws GameActionException {
        RobotInfo ri = this.rc.senseRobotAtLocation(ml);
        if (ri == null) {
            return null;
        }
        if (ri.team != myTeam) {
            return null;
        }
        if (!Bot.isUnitSpawningStructure(ri.type)) {
            return null;
        }
        return ri.type;
    }
    
    public static boolean isAirType(RobotType type) {
        switch (type) {
            case MISSILE:
            case DRONE:
                return true;
            default:
                return false;
        }
    }
}
