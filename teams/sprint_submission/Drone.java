package sprint_submission;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Drone extends NormalUnit {

    public Drone(RobotController rc) {
        super(rc);
    }

    public void performUnitSpecificActions() throws GameActionException {
    }
    
    protected Direction getNonCombatMovementDirection(SensorInfo sensorInfo, DirectionMap dm) throws GameActionException {
        MapLocation enemyTarget = this.com.getAttackTarget();
        if (enemyTarget == null) {
            enemyTarget = enemyHQLocation;
        }
        
        this.nav.setGoal(enemyTarget);
        return this.nav.getMovementDirection(this.dm);
    }
}
