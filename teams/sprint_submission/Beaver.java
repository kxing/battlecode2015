package sprint_submission;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Beaver extends NormalUnit {
    
    public Beaver(RobotController rc) {
        super(rc);
    }
    
    public void performUnitSpecificActions() throws GameActionException {
    }
    
    protected Direction getNonCombatMovementDirection(SensorInfo sensorInfo, DirectionMap dm) throws GameActionException {
        int beaverGoal = this.com.getBeaverGoal();
        if (beaverGoal == Com.BEAVER_GOAL_GO_MINING) {
            this.rc.mine();
        } else {
            RobotType type = Com.beaverGoalToRobotType(beaverGoal);
            MapLocation buildLocation = this.com.getProposedStructureLocation(type);
            if (buildLocation != null) {
                if (this.myLocation.isAdjacentTo(buildLocation)) {
                    this.tryBuild(type, buildLocation);
                    return Direction.NONE;
                }

                this.nav.setGoal(buildLocation);
                return this.nav.getMovementDirection(this.dm);
            }
        }
        return Direction.NONE;
    }
    
    public boolean tryBuild(RobotType type, MapLocation location) throws GameActionException {
        if (!this.myLocation.isAdjacentTo(location)) {
            return false;
        }
        
        if (!rc.isCoreReady() || !rc.hasBuildRequirements(type)) {
            return false;
        }
        
        Direction directionToBuild = this.myLocation.directionTo(location);
        if (rc.canBuild(directionToBuild, type)) {
            rc.build(directionToBuild, type);
            return true;
        }
        return false;
    }
}
