package sprint_submission;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public abstract class Structure extends Bot {
    private final Direction overallDirectionToEnemy;
    
    public Structure(RobotController rc) {
        super(rc);
        
        this.myLocation = rc.getLocation();
        this.overallDirectionToEnemy = myLocation.directionTo(enemyHQLocation);
    }
    
    public abstract void performUnitSpecificActions() throws GameActionException;
    
    public void takeTurn() throws GameActionException {
        this.performUnitSpecificActions();
        
        // Build units, that the HQ told to build, if necessary.
        if (Structure.isBuilderBot(myType)) {
            RobotType buildGoal = this.com.getStructureGoal(myType);
            if (buildGoal != null) {
                boolean success = this.trySpawn(buildGoal, this.overallDirectionToEnemy);

                if (success) {
                    this.com.setStructureGoal(myType, null);
                }
            }
        }
    }
    
    private static boolean isBuilderBot(RobotType type) {
        switch (type) {
            case MINERFACTORY:
            case TECHNOLOGYINSTITUTE:
            case TRAININGFIELD:
            case BARRACKS:
            case TANKFACTORY:
            case HELIPAD:
            case AEROSPACELAB:
                return true;
            default:
                return false;
        }
        
    }
}
