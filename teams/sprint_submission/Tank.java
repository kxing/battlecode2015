package sprint_submission;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Tank extends NormalUnit {

    public Tank(RobotController rc) {
        super(rc);
    }

    public void performUnitSpecificActions() throws GameActionException {
    }

    protected Direction getNonCombatMovementDirection(SensorInfo sensorInfo, DirectionMap dm) throws GameActionException {
        MapLocation enemyTarget = this.com.getAttackTarget();
        if (enemyTarget == null) {
            enemyTarget = enemyHQLocation;
        }
        if (this.rc.getSupplyLevel() == 0) {
            this.nav.setGoal(myHQLocation);
            return this.nav.getMovementDirection(this.dm);
        }
        Direction directionToObjective = myLocation.directionTo(enemyTarget);
        if (myLocation.add(directionToObjective).distanceSquaredTo(enemyTarget) <=
                RobotType.TOWER.attackRadiusSquared) {
            int ignoreDamageRound = this.com.getIgnoreAttackDamage();
            int roundNumber = Clock.getRoundNum();
            final int OVERPOWERING_MINIMUM = 12;
            
            if (sensorInfo.myInfos.length >= sensorInfo.enemyInfos.length + OVERPOWERING_MINIMUM) {
                if (roundNumber > ignoreDamageRound) {
                    this.com.setIgnoreAttackDamage(roundNumber);
                    ignoreDamageRound = roundNumber;
                }
            }
            
            if (myLocation.distanceSquaredTo(enemyTarget) <= myType.attackRadiusSquared) {
                // We can already shoot the target, so we'll stay put.
                return Direction.NONE;
            }
            
            if (ignoreDamageRound + Bot.IGNORE_DAMAGE_ROUNDS >= roundNumber) {
                // TODO(kxing): Move in the closest direction possible.
                for (Direction d: new Direction[]{directionToObjective, directionToObjective.rotateLeft(), directionToObjective.rotateRight()}) {
                    if (this.rc.canMove(d)) {
                        return d;
                    }
                }
            }
            // Don't move if you don't have to.
            return Direction.NONE;
        }
        
        this.nav.setGoal(enemyTarget);
        return this.nav.getMovementDirection(this.dm);
    }
    
}
