package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public abstract class SuicideUnit extends Bot {

    public SuicideUnit(RobotController rc) {
        super(rc);
    }

    public void takeTurn() throws GameActionException {
        // Update location, as necessary.
        this.myLocation = rc.getLocation();
        
        this.makeMove();
    }
    
    protected abstract void makeMove() throws GameActionException;
    
}
