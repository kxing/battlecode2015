package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class SensorInfo {
    public final RobotInfo[] myInfos;
    public final RobotInfo[] enemyInfos;
    
    public SensorInfo(RobotInfo[] myInfos, RobotInfo[] enemyInfos) {
        this.myInfos = myInfos;
        this.enemyInfos = enemyInfos;
    }
}
