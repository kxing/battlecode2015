package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Nav {
    private final RobotController rc;
    private final RobotType myType;
    
    private MapLocation goal;
    private boolean isBugging;
    
    // Variables for bug nav.
    private boolean bugClockwise;
    private boolean shouldResumeBugDirection;
    private int minimumUnbugDistance;
    private MapLocation furthestBuggingLocation;
    
    private MapLocation currentWallLocation;
    
    private static class BugMovement {
        public MapLocation wallLocation;
        public Direction movementDirection;
        
        public BugMovement(MapLocation wallLocation, Direction movementDirection) {
            this.wallLocation = wallLocation;
            this.movementDirection = movementDirection;
        }
    }
    
    public Nav(RobotController rc) {
        this.rc = rc;
        this.myType = rc.getType();
        this.isBugging = false;
    }
    
    public void setGoal(MapLocation goal) {
        if (!goal.equals(this.goal)) {
            this.isBugging = false;
            this.shouldResumeBugDirection = false;
        }
        this.goal = goal;
    }
    
    public MapLocation getCurrentGoal() {
        return this.goal;
    }
    
    public Direction getMovementDirection(DirectionMap dm) throws GameActionException {
        MapLocation myLocation = this.rc.getLocation();
        Direction desiredDirection = myLocation.directionTo(this.goal);

        
        // TODO(kxing): Make this much, much better.
        if (!this.isBugging) {
            Direction nonBugDirection = this.getNonBugDirection(dm);
            if (nonBugDirection != null) {
                return nonBugDirection;
            }
            
            // We can't make it there normally, so we'll have to bug around.
            this.isBugging = true;
            this.currentWallLocation = myLocation.add(desiredDirection);
            if (!this.shouldResumeBugDirection) {
                this.bugClockwise = shouldBugClockwise(dm, myLocation, this.currentWallLocation);
            }
            this.minimumUnbugDistance = myLocation.distanceSquaredTo(this.goal);
            this.furthestBuggingLocation = this.goal;
        }
        
        // If we're here, we must be bugging.
        
        // Update the furthest bugging location.
        int furthestBuggingDistance = this.furthestBuggingLocation.distanceSquaredTo(this.goal);
        int currentBuggingDistance = myLocation.distanceSquaredTo(this.goal);
        if (currentBuggingDistance < furthestBuggingDistance) {
            this.furthestBuggingLocation = myLocation;
        } else if (myLocation.equals(this.furthestBuggingLocation)) {
            // Stop bugging and try to start over, since we've been here before.
            this.isBugging = false;
            Direction nonBugDirection = this.getNonBugDirection(dm);
            if (nonBugDirection != null) {
                return nonBugDirection;
            }
            return Direction.NONE;
        }
        
        // See if we can stop bugging.
        if (myLocation.distanceSquaredTo(this.goal) < this.minimumUnbugDistance) {
            Direction[] goodDirections = new Direction[] {
                desiredDirection,
                desiredDirection.rotateLeft(),
                desiredDirection.rotateRight(),
            };
            for (Direction d: goodDirections) {
                if (this.rc.canMove(d) && !dm.isMoveBad(d)) {
                    this.isBugging = false;
                    return d;
                }
            }
        }
        
        // Check if we are bugging in the wrong direction.
        if (this.isBugging) {
            if (this.isBuggingInWrongDirection(dm, this.goal, myLocation, this.currentWallLocation, this.bugClockwise)) {
                this.bugClockwise = !this.bugClockwise;
                this.furthestBuggingLocation = this.goal;
                this.shouldResumeBugDirection = true;
            }
        }
        
        if (this.isBugging) {
            Direction directionToWall = myLocation.directionTo(this.currentWallLocation);
            if (this.rc.canMove(directionToWall) && !dm.isMoveBad(directionToWall)) {
                Direction directionToGoal = this.currentWallLocation.directionTo(this.goal);
                this.currentWallLocation = this.currentWallLocation.add(directionToGoal);
                // The wall cleared.
                return directionToWall;
            }
            
            // Move around the wall.
            BugMovement bugMovement = this.getBugMovement(dm, this.bugClockwise, myLocation, this.currentWallLocation);
            if (bugMovement != null) {
                this.currentWallLocation = bugMovement.wallLocation;
                return bugMovement.movementDirection;
            }
        }
        
        return Direction.NONE;
    }
    
    private boolean shouldBugClockwise(DirectionMap dm, MapLocation currentLocation, MapLocation wallLocation) throws GameActionException {
        // TODO(kxing): Make this smarter.
        MapLocation clockwiseLocation = currentLocation;
        MapLocation clockwiseWallLocation = wallLocation;
        MapLocation counterclockwiseLocation = currentLocation;
        MapLocation counterclockwiseWallLocation = wallLocation;
        
        for (int i = 1; i <= 5; i++) {
            BugMovement clockwiseMovement = this.getBugMovement(dm, true, clockwiseLocation, clockwiseWallLocation);
            if (clockwiseMovement == null) {
                // Can't get anywhere bugging clockwise.
                return false;
            }
            clockwiseLocation = clockwiseLocation.add(clockwiseMovement.movementDirection);
            clockwiseWallLocation = clockwiseMovement.wallLocation;
            if (clockwiseLocation.equals(this.goal)) {
                // We can get there.
                return true;
            }
            
            BugMovement counterClockwiseMovement = this.getBugMovement(dm, false, counterclockwiseLocation, counterclockwiseWallLocation);
            if (counterClockwiseMovement == null) {
                // Can't get anywhere bugging counterclockwise.
                return true;
            }
            counterclockwiseLocation = counterclockwiseLocation.add(counterClockwiseMovement.movementDirection);
            counterclockwiseWallLocation = counterClockwiseMovement.wallLocation;
            if (clockwiseLocation.equals(this.goal)) {
                // We can get there.
                return false;
            }
        }
        return true;
    }
    
    // Returns null if there is no direction that is good for moving straight.
    private Direction getNonBugDirection(DirectionMap dm) {
        MapLocation myLocation = this.rc.getLocation();
        Direction desiredDirection = myLocation.directionTo(this.goal);
        Direction[] goodDirections = new Direction[] {
            desiredDirection,
            desiredDirection.rotateLeft(),
            desiredDirection.rotateRight(),
        };
        
        for (Direction d: goodDirections) {
            if (this.rc.canMove(d) && !dm.isMoveBad(d)) {
                return d;
            }
        }

        return null;
    }
    
    private BugMovement getBugMovement(DirectionMap dm, boolean clockwise, MapLocation currentLocation, MapLocation wallLocation) throws GameActionException {
        Direction directionToWall = currentLocation.directionTo(wallLocation);
        MapLocation newWallLocation = wallLocation;
        
        Direction consideredDirection = rotateDirection(directionToWall, !clockwise);
        
        while (consideredDirection != directionToWall) {
            MapLocation consideredLocation = currentLocation.add(consideredDirection);
            if (this.isOccupiable(consideredLocation) && !dm.isMoveBad(consideredDirection)) {
                return new BugMovement(newWallLocation, consideredDirection);
            }
            consideredDirection = rotateDirection(consideredDirection, !clockwise);
            newWallLocation = consideredLocation;
        }
        
        return null;
    }
    
    private boolean isOccupiable(MapLocation ml) throws GameActionException {
        TerrainTile tile = this.rc.senseTerrainTile(ml);
        if (tile == TerrainTile.OFF_MAP) {
            return false;
        }
        // TODO(kxing): Inline this.
        if (!Bot.isAirType(myType) && tile == TerrainTile.VOID) {
            return false;
        }
        if (this.rc.canSenseLocation(ml) && this.rc.isLocationOccupied(ml)) {
            return false;
        }
        return true;
    }
    
    private boolean isBuggingInWrongDirection(
            DirectionMap dm, MapLocation goal, MapLocation currentLocation,
            MapLocation wallLocation, boolean clockwise) throws GameActionException {
        
        Direction directionToWall = currentLocation.directionTo(wallLocation);
        MapLocation nextLocation = wallLocation;
        
        while (this.rc.senseTerrainTile(nextLocation) == TerrainTile.VOID) {
            nextLocation = nextLocation.add(directionToWall);
        }
        
        if (this.rc.senseTerrainTile(nextLocation) == TerrainTile.OFF_MAP) {
            int currentDistance = currentLocation.distanceSquaredTo(goal);
            Direction nextBugDirection = this.getBugMovement(dm, clockwise, currentLocation, wallLocation).movementDirection;
            MapLocation nextBugLocation = currentLocation.add(nextBugDirection);
            int nextDistance = nextBugLocation.distanceSquaredTo(goal);
            
            if (currentDistance < nextDistance) {
                return true;
            }
        }
        return false;
    }
    
    private static Direction rotateDirection(Direction d, boolean clockwise) {
        if (clockwise) {
            return d.rotateRight();
        } else {
            return d.rotateLeft();
        }
    }
}
