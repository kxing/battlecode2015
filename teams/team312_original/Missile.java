package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Missile extends SuicideUnit {

    public Missile(RobotController rc) {
        super(rc);
    }

    protected void makeMove() throws GameActionException {
        MapLocation enemyTarget = this.com.getAttackTarget();
        if (enemyTarget == null) {
            this.rc.disintegrate();
            return;
        }
        
        if (!this.rc.isCoreReady()) {
            return;
        }
        
        // TODO(kxing): Make this much, much better.
        Direction desiredDirection = myLocation.directionTo(enemyTarget);
        Direction[] directions = new Direction[] {
            desiredDirection,
            desiredDirection.rotateLeft(),
            desiredDirection.rotateRight(),
            desiredDirection.rotateLeft().rotateLeft(),
            desiredDirection.rotateRight().rotateRight(),
            desiredDirection.opposite().rotateRight(),
            desiredDirection.opposite().rotateLeft(),
            desiredDirection.opposite(),
        };
        
        for (Direction d: directions) {
            if (this.rc.canMove(d)) {
                this.rc.move(d);
                return;
            }
        }
    }
    
}
