package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class DirectionMap {
    // Used for signalling good and bad squares for movements.
    
    public static final Direction[] POSSIBLE_DIRECTIONS = new Direction[] {
        Direction.NORTH,
        Direction.NORTH_EAST,
        Direction.EAST,
        Direction.SOUTH_EAST,
        Direction.SOUTH,
        Direction.SOUTH_WEST,
        Direction.WEST,
        Direction.NORTH_WEST,
        Direction.NONE,
    };
    
    public static final double ILLEGAL_MOVE = -1000000.0;
    
    private double[] weights;
    
    public DirectionMap() {
        this.weights = new double[POSSIBLE_DIRECTIONS.length];
    }
    
    public void clear() {
        this.weights = new double[POSSIBLE_DIRECTIONS.length];
    }
    
    public void addWeight(Direction d, double weight) {
        this.weights[DirectionMap.directionToInt(d)] += weight;
    }
    
    public Direction getBestDirection() {
        Direction bestDirection = null;
        double bestWeight = Double.NEGATIVE_INFINITY;
        
        for (int i = 0; i < POSSIBLE_DIRECTIONS.length; i++) {
            if (bestWeight < this.weights[i]) {
                bestWeight = this.weights[i];
                bestDirection = DirectionMap.intToDirection(i);
            }
        }
        
        return bestDirection;
    }
    
    public boolean isMoveBad(Direction d) {
        return this.weights[DirectionMap.directionToInt(d)] < 0.0;
    }
    
    public boolean isMoveForced() {
        // If one move is particularly good, then the move should be forced.
        for (double weight: this.weights) {
            if (weight > 0.0) {
                return true;
            }
        }
        
        // If all moves aren't great, then the move is also forced.
        // Otherwise, the move isn't forced.
        int okayMoves = 0;
        for (double weight: this.weights) {
            if (weight == 0.0) {
                okayMoves++;
            }
        }
        
        return (okayMoves <= 1);
    }
    
    private static int directionToInt(Direction d) {
        switch (d) {
            case NORTH:
                return 0;
            case NORTH_EAST:
                return 1;
            case EAST:
                return 2;
            case SOUTH_EAST:
                return 3;
            case SOUTH:
                return 4;
            case SOUTH_WEST:
                return 5;
            case WEST:
                return 6;
            case NORTH_WEST:
                return 7;
            case NONE:
                return 8;
            default:
                throw new AssertionError("Invalid direction: " + d.toString());
        }
    }
    
    private static Direction intToDirection(int i) {
        return POSSIBLE_DIRECTIONS[i];
    }
}
