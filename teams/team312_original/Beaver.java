package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Beaver extends NormalUnit {
    
    public Beaver(RobotController rc) {
        super(rc);
    }
    
    public void performUnitSpecificActions(SensorInfo sensorInfo) throws GameActionException {
    }
    
    protected boolean isPassiveUnit() {
        return true;
    }
    
    protected Direction getNonCombatMovementDirection(SensorInfo sensorInfo, DirectionMap dm) throws GameActionException {
        int beaverGoal = this.com.getBeaverGoal();
        if (beaverGoal == Com.BEAVER_GOAL_GO_MINING) {
            this.rc.mine();
        } else {
            RobotType type;
            MapLocation buildLocation;
            if (beaverGoal == Com.BEAVER_GOAL_BUILD_SUPPLIER) {
                type = RobotType.SUPPLYDEPOT;
                buildLocation = this.com.getSupplierLocation();
            } else {
                type = Com.beaverGoalToRobotType(beaverGoal);
                buildLocation = this.com.getProposedStructureLocation(type);
            }
            if (buildLocation != null) {
                if (myLocation.equals(buildLocation)) {
                    // Move off the build square.
                    for (Direction d: Bot.ALL_DIRECTIONS) {
                        if (this.rc.canMove(d) && !this.dm.isMoveBad(d)) {
                            return d;
                        }
                    }
                }
                if (myLocation.isAdjacentTo(buildLocation) && sensorInfo.enemyInfos.length == 0) {
                    // (Don't build if there's an enemy right nearby.)
                    this.tryBuild(type, buildLocation);
                    return Direction.NONE;
                }

                this.nav.setGoal(buildLocation);
                return this.nav.getMovementDirection(this.dm);
            }
        }
        return Direction.NONE;
    }
    
    public boolean tryBuild(RobotType type, MapLocation location) throws GameActionException {
        if (!this.myLocation.isAdjacentTo(location)) {
            return false;
        }
        
        if (!rc.isCoreReady() || !rc.hasBuildRequirements(type)) {
            return false;
        }
        
        Direction directionToBuild = this.myLocation.directionTo(location);
        if (rc.canBuild(directionToBuild, type)) {
            rc.build(directionToBuild, type);
            return true;
        }
        return false;
    }
}
