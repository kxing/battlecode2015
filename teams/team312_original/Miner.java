package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Miner extends NormalUnit {
    
    private static final double ORE_THRESHOLD = GameConstants.MINER_MINE_RATE * GameConstants.MINER_MINE_MAX;
    private static final double LAZY_MINING_THRESHOLD = ORE_THRESHOLD / 2;
    private static final int CLOSE_TO_MINING_GOAL = 8;
    
    public Miner(RobotController rc) {
        super(rc);
    }

    public void performUnitSpecificActions(SensorInfo sensorInfo) throws GameActionException {
        this.respondToMinerPoll();
    }
    
    protected boolean isPassiveUnit() {
        return true;
    }
    
    protected Direction getNonCombatMovementDirection(SensorInfo sensorInfo, DirectionMap dm) throws GameActionException {
        if (this.rc.senseOre(this.myLocation) >= Miner.LAZY_MINING_THRESHOLD) {
            this.rc.mine();
            return Direction.NONE;
        }
        
        MapLocation localMiningGoal = this.getLocalMiningGoal();
        
        if (localMiningGoal != null) {
            this.nav.setGoal(localMiningGoal);
            return this.nav.getMovementDirection(dm);
        }
        
        MapLocation minerGoal = this.com.getMinerGoal();
        if (minerGoal == null) {
            minerGoal = this.myHQLocation;
        } else if (myLocation.distanceSquaredTo(minerGoal) <= CLOSE_TO_MINING_GOAL) {
            // We can't find the ore that was supposed to be here.
            this.com.setMinerGoal(null);
        }
        
        this.nav.setGoal(minerGoal);
        return this.nav.getMovementDirection(dm);
    }
    
    private MapLocation getLocalMiningGoal() throws GameActionException {
        MapLocation currentGoal = this.nav.getCurrentGoal();
        if (currentGoal != null && this.rc.senseOre(currentGoal) >= Miner.ORE_THRESHOLD &&
                this.rc.canSenseLocation(currentGoal) && this.rc.senseRobotAtLocation(currentGoal) == null) {
            return currentGoal;
        }
        
        MapLocation closestLocation = null;
        int bestDistance = Integer.MAX_VALUE;
        
        for (int dx = -MINER_SEARCH_RADIUS; dx <= MINER_SEARCH_RADIUS; dx++) {
            for (int dy = -MINER_SEARCH_RADIUS; dy <= MINER_SEARCH_RADIUS; dy++) {
                MapLocation ml = this.myLocation.add(dx, dy);
                if (this.rc.senseOre(ml) >= Miner.ORE_THRESHOLD &&
                        this.rc.senseRobotAtLocation(ml) == null) {
                    int distance = dx * dx + dy * dy;
                    if (bestDistance > distance) {
                        bestDistance = distance;
                        closestLocation = ml;
                    }
                }
            }
        }
        return closestLocation;
    }

    private void respondToMinerPoll() throws GameActionException {
        if (this.com.isMinerPolling()) {
            this.com.registerMiner();
        }
    }
}
