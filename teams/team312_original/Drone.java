package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Drone extends NormalUnit {
    private static final int SQUARES_PER_GOOD_LOCATION = (2 * MINER_SEARCH_RADIUS + 1) * (2 * MINER_SEARCH_RADIUS + 1);
    private static final int GOOD_LOCATION_SPACING = (2 * MINER_SEARCH_RADIUS + 1) * (2 * MINER_SEARCH_RADIUS + 1);
    private static final double HIGH_ORE_VALUE = GameConstants.MINER_MINE_MAX;

    public Drone(RobotController rc) {
        super(rc);
    }

    public void performUnitSpecificActions(SensorInfo sensorInfo) throws GameActionException {
        updateGoodMiningLocations();
    }
    
    protected Direction getNonCombatMovementDirection(SensorInfo sensorInfo, DirectionMap dm) throws GameActionException {
        MapLocation enemyTarget = this.com.getScoutingGoal();
        if (enemyTarget == null) {
            enemyTarget = enemyHQLocation;
        }
        
        this.nav.setGoal(enemyTarget);
        return this.nav.getMovementDirection(this.dm);
    }
    
    private void updateGoodMiningLocations() throws GameActionException {
        double oreSum = 0.0;
        for (int dx = -MINER_SEARCH_RADIUS; dx <= MINER_SEARCH_RADIUS; dx++) {
            for (int dy = -MINER_SEARCH_RADIUS; dy <= MINER_SEARCH_RADIUS; dy++) {
                MapLocation ml = this.myLocation.add(dx, dy);
                oreSum += this.rc.senseOre(ml);
            }
        }
        
        if (oreSum < HIGH_ORE_VALUE * SQUARES_PER_GOOD_LOCATION) {
            // Not a lot of ore here.
            return;
        }
        // There's a lot of ore here.
        int numGoodMiningLocations = this.com.getNumberOfGoodMinerLocations();
        if (numGoodMiningLocations >= Com.MINER_MAX_NUM_GOOD_LOCATIONS) {
            // We've already found a lot of good locations.
            return;
        }
        for (int i = 0; i < numGoodMiningLocations; i++) {
            MapLocation ml = this.com.getGoodMinerLocation(i);
            if (myLocation.distanceSquaredTo(ml) <= GOOD_LOCATION_SPACING) {
                // We've basically already noted it.
                return;
            }
        }
        this.com.setGoodMinerLocation(numGoodMiningLocations, myLocation);
        this.com.setNumberOfGoodMinerLocations(numGoodMiningLocations + 1);
    }
}
