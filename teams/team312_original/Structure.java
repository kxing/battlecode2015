package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public abstract class Structure extends Bot {
    private static final int COMPUTATION_GRAIN_SIZE = 500;
    
    // By default, all structures will record terrain tiles for all squares they can sense.
    private final Direction overallDirectionToEnemy;
    protected MapLocation[] terrainTilesToRecord;
    protected int numTerrainTilesRecorded;
    
    public Structure(RobotController rc) {
        super(rc);
        
        this.myLocation = rc.getLocation();
        this.overallDirectionToEnemy = myLocation.directionTo(enemyHQLocation);
        this.terrainTilesToRecord = MapLocation.getAllMapLocationsWithinRadiusSq(myLocation, myType.sensorRadiusSquared);
        this.numTerrainTilesRecorded = 0;
    }
    
    protected abstract void performUnitSpecificActions() throws GameActionException;

    protected boolean doComputationJobGrain() throws GameActionException {
        // Can be overwritten for other tasks, but logging terrain tiles is default.
        if (this.numTerrainTilesRecorded < this.terrainTilesToRecord.length) {
            this.recordTerrainTile(this.terrainTilesToRecord[this.numTerrainTilesRecorded]);
            this.numTerrainTilesRecorded++;
            return (this.numTerrainTilesRecorded < this.terrainTilesToRecord.length);
        }
        return false;
    }
    
    public void takeTurn() throws GameActionException {
        this.performUnitSpecificActions();
        
        // Build units, that the HQ told to build, if necessary.
        if (Structure.isBuilderBot(myType)) {
            RobotType buildGoal = this.com.getStructureGoal(myType);
            if (buildGoal != null) {
                boolean success = this.trySpawn(buildGoal, this.overallDirectionToEnemy);

                if (success) {
                    this.com.setStructureGoal(myType, null);
                }
            }
        }
        
        while (Clock.getBytecodesLeft() >= Structure.COMPUTATION_GRAIN_SIZE) {
            int startBytecodeNum = Clock.getBytecodeNum();
            boolean more = this.doComputationJobGrain();
            int endBytecodeNum = Clock.getBytecodeNum();
            if (endBytecodeNum - startBytecodeNum > Structure.COMPUTATION_GRAIN_SIZE) {
                System.err.println("Batch job ran for too many bytecodes: " + (endBytecodeNum - startBytecodeNum));
            }
            if (!more) {
                break;
            }
        }
    }
    
    private static boolean isBuilderBot(RobotType type) {
        switch (type) {
            case MINERFACTORY:
            case TECHNOLOGYINSTITUTE:
            case TRAININGFIELD:
            case BARRACKS:
            case TANKFACTORY:
            case HELIPAD:
            case AEROSPACELAB:
                return true;
            default:
                return false;
        }
    }

}
