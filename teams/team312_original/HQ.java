package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class HQ extends Structure {
    
    // The period of polling for the number of miners.
    public static final int MINER_COUNT_POLL_PERIOD = 10;
    // The minimum number of miners to have at any point.
    public static final int MINIMUM_MINERS = 10;
    
    public static final int TOO_MUCH_ORE = 3000;
    
    private MapLocation latestMinerGoal;
    private MapLocation latestScoutingGoal;
    
    public HQ(RobotController rc) {
        super(rc);
    }
    
    public void performUnitSpecificActions() throws GameActionException {
        if (Clock.getRoundNum() == 0) {
            this.doFirstRoundStuff();
        }
        SensorInfo sensorInfo = this.sense();
        this.attack(sensorInfo);
        
        updateBeaverGoal();
        if (this.com.getBeaverGoal() != Com.BEAVER_GOAL_GO_MINING &&
                !this.isBeaverInSight(sensorInfo)) {
            Direction directionToEnemy = myHQLocation.directionTo(enemyHQLocation);
            this.trySpawn(RobotType.BEAVER, directionToEnemy);
        }
        
        updateMinerGoal();
        updateScoutingGoal();
        updateRallyLocation();
        
        this.transferSupplies(sensorInfo);
        
        this.monitorNumberOfMiners();
        
        if (this.rc.getTeamOre() >= 2 * RobotType.DRONE.oreCost && this.com.getStructureGoal(RobotType.HELIPAD) != RobotType.DRONE &&
                Clock.getRoundNum() <= 500) {
            this.com.setStructureGoal(RobotType.HELIPAD, RobotType.DRONE);
        }
        
//        if (this.rc.getTeamOre() >= 2 * RobotType.LAUNCHER.oreCost && this.com.getStructureGoal(RobotType.AEROSPACELAB) != RobotType.LAUNCHER) {
//            this.com.setStructureGoal(RobotType.AEROSPACELAB, RobotType.LAUNCHER);
//        }
        if (this.rc.getTeamOre() >= 2 * RobotType.TANK.oreCost && this.com.getStructureGoal(RobotType.TANKFACTORY) != RobotType.TANK) {
            this.com.setStructureGoal(RobotType.TANKFACTORY, RobotType.TANK);
        }
        
        this.updateMilitaryObjectives();
    }
    
    private void transferSupplies(SensorInfo si) throws GameActionException {
        final int SUPPLY_ROUND_MINIMUM = 500;
        double supplyLevelLeft = this.rc.getSupplyLevel();
        boolean needsMoreSupply = false;
        for (RobotInfo ri: si.myInfos) {
            int desiredSupply = ri.type.supplyUpkeep * SUPPLY_ROUND_MINIMUM;
            if (ri.supplyLevel < desiredSupply &&
                    myLocation.distanceSquaredTo(ri.location) <= GameConstants.SUPPLY_TRANSFER_RADIUS_SQUARED) {
                double toTransfer = Math.min(desiredSupply - ri.supplyLevel, supplyLevelLeft / 2);
                if (toTransfer < desiredSupply - ri.supplyLevel) {
                    needsMoreSupply = true;
                }
                this.rc.transferSupplies((int)(toTransfer), ri.location);
                supplyLevelLeft -= (int)(toTransfer);
                
            }
        }
        if (needsMoreSupply && Clock.getRoundNum() >= 200 && this.com.getBeaverGoal() != Com.BEAVER_GOAL_BUILD_SUPPLIER) {
            this.com.setBeaverGoal(Com.BEAVER_GOAL_BUILD_SUPPLIER);
            this.com.setSupplierLocation(this.getStructureLocation());
        }
    }
    
    private MapLocation getStructureLocation() throws GameActionException {
        int x = myHQLocation.x;
        int y = myHQLocation.y;
        
        for (int radius = 1; radius <= 3; radius++) {
            // Try looking for squares within the radius.
            for (int dx = -radius; dx <= radius; dx++) {
                for (int offset : new int[]{radius, -radius}) {
                    MapLocation candidateLocation = new MapLocation(x + dx, y + offset);
                    if (isGoodBuildLocation(candidateLocation)) {
                        return candidateLocation;
                    }
                }
            }
            for (int dy = -radius; dy <= radius; dy++) {
                for (int offset : new int[]{radius, -radius}) {
                    MapLocation candidateLocation = new MapLocation(x + offset, y + dy);
                    if (isGoodBuildLocation(candidateLocation)) {
                        return candidateLocation;
                    }
                }
            }
        }
        return null;
    }
    
    private boolean isGoodBuildLocation(MapLocation ml) throws GameActionException {
        if (this.rc.senseTerrainTile(ml) != TerrainTile.NORMAL || this.getStructureAt(ml) != null) {
            // Something else is already built here.
            return false;
        }
        // A building location is good, if it doesn't disconnect reachable squares, and if its
        // adjacent structures still have somewhere to build.
        
        boolean[] blocked = new boolean[ALL_DIRECTIONS.length];
        for (int i = 0; i < ALL_DIRECTIONS.length; i++) {
            Direction d = ALL_DIRECTIONS[i];
            MapLocation adjacentLocation = ml.add(d);
            if (this.rc.senseTerrainTile(adjacentLocation) != TerrainTile.NORMAL ||
                    this.getStructureAt(adjacentLocation) != null) {
                blocked[i] = true;
            }
        }
        
        boolean currentlyBlocked = blocked[0];
        int lastTF = -1;
        int lastFT = -1;
        for (int i = 1; i < blocked.length; i++) {
            if (currentlyBlocked && !blocked[i]) {
                if (lastTF != -1) {
                    return false;
                }
                lastTF = i;
                currentlyBlocked = false;
            } else if (!currentlyBlocked && blocked[i]) {
                if (lastFT != -1) {
                    return false;
                }
                lastFT = i;
                currentlyBlocked = true;
            }
        }
        
        // Check that adjacent structures have somewhere to spawn.
        MapLocation[] adjacentStructures = new MapLocation[ALL_DIRECTIONS.length];
        int numAdjacentStructures = 0;
        for (Direction d : ALL_DIRECTIONS) {
            MapLocation adjacentLocation = ml.add(d);
            if (this.getStructureAt(adjacentLocation) != null) {
                adjacentStructures[numAdjacentStructures++] = adjacentLocation;
            }
        }
        
        for (int i = 0; i < numAdjacentStructures; i++) {
            MapLocation adjacentLocation = adjacentStructures[i];
            boolean canSpawn = false;
            for (Direction d: ALL_DIRECTIONS) {
                MapLocation potentialSpawnSquare = adjacentLocation.add(d);
                boolean spawnableSquare = (this.rc.senseTerrainTile(potentialSpawnSquare) == TerrainTile.NORMAL);
                boolean unoccupied = (this.getStructureAt(potentialSpawnSquare) == null);
                if (spawnableSquare && unoccupied) {
                    canSpawn = true;
                    break;
                }
            }
            if (!canSpawn) {
                return false;
            }
        }
        
        return true;
    }
    
    private boolean isStructureBuilt(RobotType type) throws GameActionException {
        MapLocation ml = this.com.getProposedStructureLocation(type);
        if (ml == null) {
            return false;
        }
        return (this.getStructureAt(ml) == type);
    }
    
    private boolean isBeaverInSight(SensorInfo sensorInfo) {
        for (RobotInfo ri: sensorInfo.myInfos) {
            if (ri.team == myTeam && ri.type == RobotType.BEAVER) {
                return true;
            }
        }
        return false;
    }
    
    private void updateBeaverGoal() throws GameActionException {
        int desiredBeaverGoal = Com.BEAVER_GOAL_GO_MINING;
        int currentBeaverGoal = this.com.getBeaverGoal();
        
        // TODO(kxing): Make this better.
        RobotType structureToBuild = null;
        if (!this.isStructureBuilt(RobotType.MINERFACTORY)) {
            desiredBeaverGoal = Com.BEAVER_GOAL_BUILD_MINER_FACTORY;
            structureToBuild = RobotType.MINERFACTORY;
        } else if (!this.isStructureBuilt(RobotType.HELIPAD)) {
            desiredBeaverGoal = Com.BEAVER_GOAL_BUILD_HELIPAD;
            structureToBuild = RobotType.HELIPAD;
        } else if (!this.isStructureBuilt(RobotType.BARRACKS)) {
            desiredBeaverGoal = Com.BEAVER_GOAL_BUILD_BARRACKS;
            structureToBuild = RobotType.BARRACKS;
        } else if (!this.isStructureBuilt(RobotType.TANKFACTORY) || this.rc.getTeamOre() >= HQ.TOO_MUCH_ORE) {
            desiredBeaverGoal = Com.BEAVER_GOAL_BUILD_TANK_FACTORY;
            structureToBuild = RobotType.TANKFACTORY;
        }
        // TODO(kxing): Check how good launchers and commanders are.
//        else if (!this.isStructureBuilt(RobotType.AEROSPACELAB)) {
//            desiredBeaverGoal = Com.BEAVER_GOAL_BUILD_AEROSPACE_LAB;
//            structureToBuild = RobotType.AEROSPACELAB;
//        }
        
        if (desiredBeaverGoal != currentBeaverGoal) {
            this.com.setBeaverGoal(desiredBeaverGoal);
            if (structureToBuild != null) {
                MapLocation ml = this.getStructureLocation();
                this.com.setProposedStructureLocation(structureToBuild, ml);
            }
        }
    }
    
    private void updateMinerGoal() throws GameActionException {
        if (this.com.getMinerGoal() == null) {
            // Update the miner goal.
            int numGoodMinerLocations = this.com.getNumberOfGoodMinerLocations();
            int bestDistance = Integer.MAX_VALUE;
            MapLocation bestLocation = null;
            int bestIndex = -1;
            for (int i = 0; i < numGoodMinerLocations; i++) {
                boolean isBetter = false;
                MapLocation potentialLocation = this.com.getGoodMinerLocation(i);
                int distance = this.latestMinerGoal.distanceSquaredTo(potentialLocation) -
                        potentialLocation.distanceSquaredTo(enemyHQLocation);
                if (bestIndex == -1) {
                    isBetter = true;
                } else if (bestDistance > distance) {
                    isBetter = true;
                }
                
                if (isBetter) {
                    bestDistance = distance;
                    bestIndex = i;
                    bestLocation = potentialLocation;
                }
            }
            
            if (bestIndex != -1) {
                this.com.setMinerGoal(bestLocation);
                this.latestMinerGoal = bestLocation;
                
                // Remove the location from the list of good locations.
                MapLocation lastLocation = this.com.getGoodMinerLocation(numGoodMinerLocations - 1);
                this.com.setGoodMinerLocation(bestIndex, lastLocation);
                this.com.setNumberOfGoodMinerLocations(numGoodMinerLocations - 1);
                return;
            }
        }
    }
    
    private void updateScoutingGoal() throws GameActionException {
        MapLocation[] enemyTowerLocations = this.rc.senseEnemyTowerLocations();
        MapLocation bestGoal = null;
        int bestDistance = Integer.MAX_VALUE;
        
        for (MapLocation ml: enemyTowerLocations) {
            boolean scouted = false;
            for (Direction d: Bot.ALL_DIRECTIONS) {
                if (this.com.getTerrainTile(ml.add(d)) != TerrainTile.UNKNOWN) {
                    scouted = true;
                    break;
                }
            }
            if (!scouted) {
                boolean isBetter = false;
                int distance = ml.distanceSquaredTo(this.latestScoutingGoal);
                if (bestGoal == null) {
                    isBetter = true;
                } else if (bestDistance > distance) {
                    isBetter = true;
                }
                
                if (isBetter) {
                    bestGoal = ml;
                    bestDistance = distance;
                }
            }
        }
        
        
        if (bestGoal == null) {
            
            Direction directionToEnemy = myLocation.directionTo(enemyHQLocation);
            Direction[] directionOrder;
            switch (directionToEnemy) {
                case NORTH:
                case NORTH_EAST:
                    directionOrder = new Direction[]{
                        Direction.SOUTH, Direction.WEST, Direction.EAST, Direction.NORTH
                    };
                    break;
                case NORTH_WEST:
                    directionOrder = new Direction[]{
                        Direction.SOUTH, Direction.EAST, Direction.WEST, Direction.NORTH
                    };
                    break;
                case SOUTH:
                case SOUTH_WEST:
                    directionOrder = new Direction[]{
                        Direction.NORTH, Direction.EAST, Direction.WEST, Direction.SOUTH
                    };
                    break;
                case SOUTH_EAST:
                    directionOrder = new Direction[]{
                        Direction.NORTH, Direction.WEST, Direction.EAST, Direction.SOUTH
                    };
                    break;
                case WEST:
                    directionOrder = new Direction[]{
                        Direction.EAST, Direction.SOUTH, Direction.NORTH, Direction.WEST
                    };
                    break;
                case EAST:
                    directionOrder = new Direction[]{
                        Direction.WEST, Direction.NORTH, Direction.SOUTH, Direction.EAST
                    };
                    break;
                default:
                    throw new AssertionError("direction not recognized: " + directionToEnemy.toString());
            }
            
            for (Direction d: directionOrder) {
                if (bestGoal != null) {
                    break;
                }
                switch (d) {
                    case NORTH:
                        if (!this.com.isMinYSet()) {
                            bestGoal = myLocation.add(d, GameConstants.MAP_MAX_HEIGHT);
                        }
                        break;
                    case SOUTH:
                        if (!this.com.isMaxYSet()) {
                            bestGoal = myLocation.add(d, GameConstants.MAP_MAX_HEIGHT);
                        }
                        break;
                    case WEST:
                        if (!this.com.isMinXSet()) {
                            bestGoal = myLocation.add(d, GameConstants.MAP_MAX_WIDTH);
                        }
                        break;
                    case EAST:
                        if (!this.com.isMaxXSet()) {
                            bestGoal = myLocation.add(d, GameConstants.MAP_MAX_WIDTH);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        
        if (bestGoal == null) {
//            this.com.debug_printMap();
            Direction directionToEnemy = this.latestMinerGoal.directionTo(enemyHQLocation);
            bestGoal = this.latestMinerGoal.add(directionToEnemy, 8);
            
        }
        
        if (!bestGoal.equals(this.com.getScoutingGoal())) {
            this.com.setScoutingGoal(bestGoal);
        }
    }
    
    private void updateRallyLocation() throws GameActionException {
        MapLocation currentRallyPoint = this.com.getRallyLocation();
        if (currentRallyPoint == null) {
            currentRallyPoint = myHQLocation;
        }
        MapLocation rallyPoint = null;
        
        final int UNDER_ATTACK_THRESHOLD = 10;
        int roundNum = Clock.getRoundNum();
        
        int bestDistance = Integer.MAX_VALUE;
        for (int i = 0; i < GameConstants.NUMBER_OF_TOWERS_MAX; i++) {
            if (this.com.getDefenseTowerUnderAttack(i) + UNDER_ATTACK_THRESHOLD >= roundNum) {
                MapLocation towerLocation = this.com.getDefenseTowerLocation(i);
                if (towerLocation == null) {
                    continue;
                }
                int distance = towerLocation.distanceSquaredTo(currentRallyPoint);
                if (bestDistance > distance) {
                    bestDistance = distance;
                    rallyPoint = towerLocation;
                }
            }
        }
        
        if (rallyPoint == null) {
            MapLocation minerGoal = this.com.getMinerGoal();
            if (minerGoal == null) {
                minerGoal = myHQLocation;
            }
            Direction directionToEnemy = minerGoal.directionTo(enemyHQLocation);
            rallyPoint = minerGoal.add(directionToEnemy, 5);
            while (this.com.getTerrainTile(rallyPoint) == TerrainTile.VOID) {
                rallyPoint = rallyPoint.add(directionToEnemy);
            }
        }
        
        if (rallyPoint != this.com.getRallyLocation()) {
            this.com.setRallyLocation(rallyPoint);
        }
    }
    
    private void monitorNumberOfMiners() throws GameActionException {
        int roundNum = Clock.getRoundNum();
        switch (roundNum % HQ.MINER_COUNT_POLL_PERIOD) {
            case 0:
                this.com.clearMinerCount();
                this.com.setMinerPolling(true);
                break;
            case 1:
                boolean shouldSpawnMiners = false;
                // Every miner should have responded to the poll.
                if (this.com.getNumberOfMiners() < HQ.MINIMUM_MINERS) {
                    shouldSpawnMiners = true;
                }
                if (this.rc.getTeamOre() >= 2000.0) {
                    shouldSpawnMiners = true;
                }
                if (this.rc.getRoundLimit() - roundNum < 100) {
                    // Don't spawn miners too close to the round limit.
                    shouldSpawnMiners = false;
                }
                if (shouldSpawnMiners && this.com.getStructureGoal(RobotType.MINERFACTORY) == null) {
                    this.com.setStructureGoal(RobotType.MINERFACTORY, RobotType.MINER);
                }
                this.com.setMinerPolling(false);
                break;
        }
    }
    
    private void doFirstRoundStuff() throws GameActionException {
        this.com.setSiegeLocation(0, enemyHQLocation);
        MapLocation[] enemyTowerLocations = this.rc.senseEnemyTowerLocations();
        for (int i = 0; i < enemyTowerLocations.length; i++) {
            this.com.setSiegeLocation(i + 1, enemyTowerLocations[i]);
        }
        MapLocation[] myTowerLocations = this.rc.senseTowerLocations();
        for (int i = 0; i < myTowerLocations.length; i++) {
            this.com.setDefenseTowerLocation(i, myTowerLocations[i]);
        }
        
        this.com.setMinerGoal(myLocation);
        this.latestMinerGoal = myLocation;
        this.latestScoutingGoal = myLocation;
    }
    
    private void updateMilitaryObjectives() throws GameActionException {
        for (int i = 0; i <= GameConstants.NUMBER_OF_TOWERS_MAX; i++) {
            if (this.com.getSiegeNetDPS(i) >= RobotType.TOWER.maxHealth * 3 / 5) {
                this.com.setSiegeTakedownRound(i, Clock.getRoundNum() + Bot.IGNORE_DAMAGE_ROUNDS);
            }
            this.com.resetSiegeNetDPS(i);
        }
    
        MapLocation currentTarget = this.com.getAttackTarget();
        MapLocation nextTarget = null;
        MapLocation[] enemyTowers = this.rc.senseEnemyTowerLocations();
        if (currentTarget == null) {
            // If we have no target, we just go for the one furthest from the enemy HQ.
            MapLocation furthestTower = enemyHQLocation;
            int furthestDistance = 0;

            for (MapLocation ml: enemyTowers) {
                int distance = ml.distanceSquaredTo(enemyHQLocation);
                if (furthestDistance < distance) {
                    furthestDistance = distance;
                    furthestTower = ml;
                }
            }
            nextTarget = furthestTower;
        } else {
            // Check if our previous target is still alive.
            for (MapLocation ml: enemyTowers) {
                if (currentTarget.equals(ml)) {
                    nextTarget = currentTarget;
                    break;
                }
            }
            
            if (nextTarget == null) {
                // If we destroyed our previous target, go to the closest one, without going for something
                // too close to the enemy HQ.
                MapLocation closestTower = enemyHQLocation;
                int closestDistance = Integer.MAX_VALUE;

                for (MapLocation ml: enemyTowers) {
                    int distance = ml.distanceSquaredTo(currentTarget) - ml.distanceSquaredTo(enemyHQLocation);
                    if (closestDistance > distance) {
                        closestDistance = distance;
                        closestTower = ml;
                    }
                }

                nextTarget = closestTower;
            }
        }
        
        if (!nextTarget.equals(currentTarget)) {
            this.com.setAttackTarget(nextTarget);
        }
    }
}
