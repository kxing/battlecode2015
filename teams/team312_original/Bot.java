package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public abstract class Bot {
    
    protected final RobotController rc;
    protected final RobotType myType;
    protected final Team myTeam;
    protected final Team enemyTeam;
    protected final MapLocation myHQLocation;
    protected final MapLocation enemyHQLocation;
    protected final Com com;
    protected final int spawnedRound;
    
    protected MapLocation myLocation;
    
    public static final int MINER_SEARCH_RADIUS = 3;
    
    public static final Direction[] ALL_DIRECTIONS = new Direction[] {
        Direction.NORTH,
        Direction.NORTH_EAST,
        Direction.EAST,
        Direction.SOUTH_EAST,
        Direction.SOUTH,
        Direction.SOUTH_WEST,
        Direction.WEST,
        Direction.NORTH_WEST,
    };
    
    public static final Direction[] ALL_DIRECTIONS_INCLUDING_NONE = new Direction[] {
        Direction.NORTH,
        Direction.NORTH_EAST,
        Direction.EAST,
        Direction.SOUTH_EAST,
        Direction.SOUTH,
        Direction.SOUTH_WEST,
        Direction.WEST,
        Direction.NORTH_WEST,
        Direction.NONE,
    };
    
    // One spot to build every unit-building structures.
    public static final RobotType[] UNIT_SPAWNING_STRUCTURES = new RobotType[] {
        RobotType.MINERFACTORY,
        RobotType.TECHNOLOGYINSTITUTE,
        RobotType.TRAININGFIELD,
        RobotType.BARRACKS,
        RobotType.TANKFACTORY,
        RobotType.HELIPAD,
        RobotType.AEROSPACELAB,
    };
    
    // For seiging.
    public static final int IGNORE_DAMAGE_ROUNDS = 20;
    
    public Bot(RobotController rc) {
        this.rc = rc;
        this.myType = rc.getType();
        this.myTeam = rc.getTeam();
        this.enemyTeam = rc.getTeam().opponent();
        this.myHQLocation = rc.senseHQLocation();
        this.enemyHQLocation = rc.senseEnemyHQLocation();
        this.com = new Com(rc);
        this.spawnedRound = Clock.getRoundNum();
        
        this.myLocation = rc.getLocation();
    }
    
    public void run() {
        while (true) {
            int lastRoundNum = Clock.getRoundNum();
            try {
                this.takeTurn();
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            int currentRoundNum = Clock.getRoundNum();
            if (currentRoundNum > lastRoundNum) {
                System.err.println("Exceeded bytecode limit");
            }
            
            this.rc.yield();
        }
    }
    
    public abstract void takeTurn() throws GameActionException;
    
    public SensorInfo sense() throws GameActionException {
        RobotInfo[] myInfos = this.rc.senseNearbyRobots(myType.sensorRadiusSquared, myTeam);
        RobotInfo[] enemyInfos = this.rc.senseNearbyRobots(myType.sensorRadiusSquared, enemyTeam);
        
        return new SensorInfo(myInfos, enemyInfos);
    }
    
    public boolean trySpawn(RobotType type, Direction direction) throws GameActionException {
        if (!this.rc.isCoreReady() || this.rc.getTeamOre() < type.oreCost) {
            return false;
        }
        
        Direction[] directions = new Direction[]{
            direction,
            direction.rotateLeft(),
            direction.rotateRight(),
            direction.rotateLeft().rotateLeft(),
            direction.rotateRight().rotateRight(),
            direction.opposite().rotateRight(),
            direction.opposite().rotateLeft(),
            direction.opposite(),
        };
        
        for (Direction d : directions) {
            if (this.rc.canSpawn(d, type)) {
                this.rc.spawn(d, type);
                return true;
            }
        }
        
        return false;
    };
    
    private static int getAttackPriority(RobotType type) {
        switch (type) {
            case HQ:
                return 12;
            case TOWER:
                return 11;
            case MISSILE:
                return 10;
            case COMMANDER:
                return 9;
            case TANK:
                return 8;
            case LAUNCHER:
                return 7;
            case BASHER:
                return 6;
            case DRONE:
                return 5;
            case SOLDIER:
                return 4;
            case BEAVER:
                return 3;
            case MINER:
                return 2;
            default:
                return 0;
        }
    }
    
    public boolean attack(SensorInfo sensorInfo) throws GameActionException {
        if (!rc.isWeaponReady()) {
            return false;
        }
        MapLocation attackTarget = this.com.getAttackTarget();
        if (attackTarget != null && myLocation.distanceSquaredTo(attackTarget) <= myType.attackRadiusSquared) {
            // Attack the tower/HQ if you can.
            this.rc.attackLocation(attackTarget);
            return true;
        }
        
        // Attack based off priority:
        // missiles > commander > tank > launcher > basher > drone > soldier > beaver > miner.
        // Attack then target HP, and then distance.
        // TODO(kxing): Make this a lot smarter.
        MapLocation bestLocation = null;
        int bestPriority = -1;
        double lowestHP = Double.MAX_VALUE;
        int closestDistance = Integer.MAX_VALUE;
        
        for (RobotInfo ri : sensorInfo.enemyInfos) {
            if (!this.rc.canAttackLocation(ri.location)) {
                continue;
            }
            
            boolean isBetter;
            int priority = Bot.getAttackPriority(ri.type);
            int distance = myLocation.distanceSquaredTo(ri.location);
            
            if (bestPriority < priority) {
                isBetter = true;
            } else if (bestPriority > priority) {
                isBetter = false;
            } else if (lowestHP > ri.health) {
                isBetter = true;
            } else if (lowestHP < ri.health) {
                isBetter = false;
            } else if (closestDistance > distance) {
                isBetter = true;
            } else {
                isBetter = false;
            }
            
            if (isBetter) {
                bestLocation = ri.location;
                bestPriority = priority;
                lowestHP = ri.health;
                closestDistance = distance;
            }
        }
        
        if (bestLocation != null) {
            rc.attackLocation(bestLocation);
            return true;
        }
        return false;
    }
    
    public static boolean isUnitSpawningStructure(RobotType type) {
        switch (type) {
            case AEROSPACELAB:
            case BARRACKS:
            case HELIPAD:
            case MINERFACTORY:
            case TANKFACTORY:
            case TECHNOLOGYINSTITUTE:
            case TRAININGFIELD:
                return true;
            default:
                return false;
        }
    }
    
    public static boolean isStructure(RobotType type) {
        return type.isBuilding;
    }
        
    public RobotType getStructureAt(MapLocation ml) throws GameActionException {
        RobotInfo ri = this.rc.senseRobotAtLocation(ml);
        if (ri == null) {
            return null;
        }
        if (ri.team != myTeam) {
            return null;
        }
        if (!Bot.isStructure(ri.type)) {
            return null;
        }
        return ri.type;
    }
    
    public static boolean isAirType(RobotType type) {
        switch (type) {
            case MISSILE:
            case DRONE:
                return true;
            default:
                return false;
        }
    }
    
    private void debug_ensureCanSense(MapLocation ml) {
        if (!this.rc.canSenseLocation(ml)) {
            throw new AssertionError("Cannot record a terrain tile for a square that cannot be sensed.");
        }
    }
    
    protected void recordTerrainTile(MapLocation ml) throws GameActionException {
        debug_ensureCanSense(ml);
        
        TerrainTile fetchedTile = this.com.getTerrainTile(ml);
        TerrainTile actualTile = this.rc.senseTerrainTile(ml);

        if (fetchedTile != actualTile) {
            this.com.setTerrainTile(ml, actualTile);
            
            if (actualTile == TerrainTile.OFF_MAP) {
                if (!this.com.isMinXSet()) {
                    MapLocation minXSquare = ml.add(1, 0);
                    if (this.rc.canSenseLocation(minXSquare) && this.rc.senseTerrainTile(minXSquare) != TerrainTile.OFF_MAP) {
                        this.com.setMinX(ml.x);
                    }
                }
                if (!this.com.isMinYSet()) {
                    MapLocation minYSquare = ml.add(0, 1);
                    if (this.rc.canSenseLocation(minYSquare) && this.rc.senseTerrainTile(minYSquare) != TerrainTile.OFF_MAP) {
                        this.com.setMinY(ml.y);
                    }
                }
                if (!this.com.isMaxXSet()) {
                    MapLocation maxXSquare = ml.add(-1, 0);
                    if (this.rc.canSenseLocation(maxXSquare) && this.rc.senseTerrainTile(maxXSquare) != TerrainTile.OFF_MAP) {
                        this.com.setMaxX(ml.x);
                    }
                }
                if (!this.com.isMaxYSet()) {
                    MapLocation maxYSquare = ml.add(0, -1);
                    if (this.rc.canSenseLocation(maxYSquare) && this.rc.senseTerrainTile(maxYSquare) != TerrainTile.OFF_MAP) {
                        this.com.setMaxY(ml.y);
                    }
                }
            }
        }
    }
    
    protected void recordOre(MapLocation ml) throws GameActionException {
        debug_ensureCanSense(ml);
        
        int fetchedOreAmount = this.com.getOreAmount(ml);
        int actualOreAmount = (int)(this.rc.senseOre(ml));
        if (fetchedOreAmount != actualOreAmount) {
            this.com.setOreAmount(ml, actualOreAmount);
        }
    }
    
    protected int siegeLocationToInt(MapLocation ml) throws GameActionException {
        for (int i = 0; i <= GameConstants.NUMBER_OF_TOWERS_MAX; i++) {
            MapLocation siegeTarget = this.com.getSiegeLocation(i);
            if (ml.equals(siegeTarget)) {
                return i;
            }
        }
        throw new AssertionError("Siege location not found: " + ml.toString());
    }
}
