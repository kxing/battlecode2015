package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Tower extends Structure {
    
    public Tower(RobotController rc) {
        super(rc);
    }

    protected void performUnitSpecificActions() throws GameActionException {
        SensorInfo sensorInfo = this.sense();
        this.attack(sensorInfo);
        
        this.callForHelp(sensorInfo);
    }
    
    private int getSelfIndex() throws GameActionException {
        for (int i = 0; i < GameConstants.NUMBER_OF_TOWERS_MAX; i++) {
            if (myLocation.equals(this.com.getDefenseTowerLocation(i))) {
                return i;
            }
        }
        throw new AssertionError("Cannot find self index");
    }
    
    private void callForHelp(SensorInfo sensorInfo) throws GameActionException {
        if (sensorInfo.enemyInfos.length > 0) {
            int roundNum = Clock.getRoundNum();
            this.com.setDefenseTowerUnderAttack(this.getSelfIndex(), roundNum);
        }
    }
}
