package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Tank extends NormalUnit {
    private enum State {
        RALLYING,
        SIEGING,
    }
    
    private State state;
    private boolean incrementedDPS;
    private static final int RALLY_RADIUS_SQUARED = RobotType.TANK.sensorRadiusSquared;
    // How close to the attack target that the unit must be, to convert to siege mode.
    private static final int SIEGE_CONVERSION_RADIUS_SQUARED = 35;

    public Tank(RobotController rc) {
        super(rc);
        
        this.state = State.RALLYING;
    }

    public void performUnitSpecificActions(SensorInfo sensorInfo) throws GameActionException {
        this.incrementedDPS = false;
        
        if (this.state == State.RALLYING) {
            MapLocation rallyLocation = this.getRallyLocation();
            int currentRoundNumber = Clock.getRoundNum();
            int currentRallyDoneRound = this.com.getRallyDoneRound();
            if (myLocation.distanceSquaredTo(rallyLocation) <= RALLY_RADIUS_SQUARED &&
                    currentRoundNumber <= currentRallyDoneRound) {
                this.state = State.SIEGING;
            }
            
            MapLocation siegeTarget = this.com.getAttackTarget();
            if (myLocation.distanceSquaredTo(siegeTarget) < SIEGE_CONVERSION_RADIUS_SQUARED) {
                this.state = State.SIEGING;
            }
            
            int numTanks = 1;
            for (RobotInfo ri: sensorInfo.myInfos) {
                if (ri.type == RobotType.TANK) {
                    numTanks++;
                }
            }

            if (numTanks >= 8) {
                if (currentRallyDoneRound != currentRoundNumber + 2) {
                    this.com.setRallyDoneRound(currentRoundNumber + 2);
                }
            }
        } else if (this.state == State.SIEGING) {
            MapLocation enemyTarget = this.com.getAttackTarget();
            if (enemyTarget != null) {
                if (isGoodSiegeSquare(myLocation, enemyTarget) &&
                        NormalUnit.haveMoreDPS(sensorInfo)) {
                    int targetIndex = this.siegeLocationToInt(enemyTarget);
                    int dps = (int)(this.rc.getHealth() / RobotType.TOWER.attackPower * RobotType.TOWER.attackDelay) *
                            (int)(myType.attackPower / myType.attackDelay);
                    this.com.incrementSiegeNetDPS(targetIndex, dps);
                    this.incrementedDPS = true;
                }
            }
        }
    }

    protected Direction getNonCombatMovementDirection(SensorInfo sensorInfo, DirectionMap dm) throws GameActionException {
        switch (this.state) {
            case RALLYING:
                return this.getRallyingMove();
            case SIEGING:
                return this.getSiegingMove(sensorInfo);
            default:
                throw new AssertionError("Not recognized: " + this.state);
        }
    }
    
    private MapLocation getRallyLocation() throws GameActionException {
        MapLocation rallyLocation = this.com.getRallyLocation();
        if (rallyLocation == null) {
            rallyLocation = myHQLocation;
        }
        return rallyLocation;
    }
    
    private Direction getRallyingMove() throws GameActionException {
        MapLocation rallyLocation = this.getRallyLocation();
        this.nav.setGoal(rallyLocation);
        return this.nav.getMovementDirection(dm);
    }
    
    private Direction getSiegingMove(SensorInfo sensorInfo) throws GameActionException {
        MapLocation enemyTarget = this.com.getAttackTarget();
        if (enemyTarget == null) {
            enemyTarget = enemyHQLocation;
        }
        Direction directionToObjective = myLocation.directionTo(enemyTarget);
        if (myLocation.distanceSquaredTo(enemyTarget) <= myType.attackRadiusSquared) {
            MapLocation[] squaresBehind = new MapLocation[] {
                myLocation.add(directionToObjective.opposite()),
                myLocation.add(directionToObjective.opposite().rotateLeft()),
                myLocation.add(directionToObjective.opposite().rotateRight()),
            };
            boolean shouldMakeRoom = false;
            for (MapLocation ml: squaresBehind) {
                RobotInfo allyBehind = this.rc.senseRobotAtLocation(ml);
                if (this.isFriendlyTank(allyBehind)) {
                    shouldMakeRoom = true;
                    break;
                }
            }
            if (shouldMakeRoom) {
                for (Direction d: new Direction[]{directionToObjective, directionToObjective.rotateLeft(), directionToObjective.rotateRight()}) {
                    if (this.rc.canMove(d) && !dm.isMoveBad(d)) {
                        return d;
                    }
                }
            }
            // We can already shoot the target, so we'll stay put.
            return Direction.NONE;
        }
        
        if (this.incrementedDPS) {
            int targetIndex = this.siegeLocationToInt(enemyTarget);
            int roundNumber = Clock.getRoundNum();
            
            if (this.com.getSiegeTakedownRound(targetIndex) >= roundNumber) {
                // TODO(kxing): Move in the closest direction possible.
                for (Direction d: new Direction[]{directionToObjective, directionToObjective.rotateLeft(), directionToObjective.rotateRight()}) {
                    if (this.rc.canMove(d)) {
                        return d;
                    }
                }
            }
            // Don't move if you don't have to.
            return Direction.NONE;
        }
        
        this.nav.setGoal(enemyTarget);
        return this.nav.getMovementDirection(this.dm);
    }
    
    private boolean isGoodSiegeSquare(MapLocation ml, MapLocation enemyTarget) throws GameActionException {
        if (!this.hasPathToAttackTarget(enemyTarget)) {
            return false;
        }
        Direction directionToObjective = ml.directionTo(enemyTarget);
        MapLocation nextSquare = ml.add(directionToObjective);
        if (nextSquare.distanceSquaredTo(enemyTarget) <= RobotType.TOWER.attackRadiusSquared) {
            return true;
        }
        RobotInfo allyAhead = this.rc.senseRobotAtLocation(nextSquare);
        if (!this.isFriendlyTank(allyAhead)) {
            return false;
        }
        return isGoodSiegeSquare(nextSquare, enemyTarget);
    }
    
    private boolean isFriendlyTank(RobotInfo ri) {
        if (ri == null) {
            return false;
        }
        if (ri.team != myTeam) {
            return false;
        }
        if (ri.type != RobotType.TANK) {
            return false;
        }
        return true;
    }
    
}
