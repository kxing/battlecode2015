package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public abstract class NormalUnit extends Bot {
    
    protected final Nav nav;
    protected final DirectionMap dm;
    
    public final static int TOWER_CLUMP_THRESHOLD = 5;
    
    private Direction lastDirection;
    
    public NormalUnit(RobotController rc) {
        super(rc);
        
        this.nav = new Nav(rc);
        this.dm = new DirectionMap();
        this.lastDirection = Direction.NONE;
    }
    
    public abstract void performUnitSpecificActions(SensorInfo sensorInfo) throws GameActionException;
    
    protected boolean isPassiveUnit() {
        // Can be overwritten for units that don't want to be in combat.
        return false;
    }
    
    public void takeTurn() throws GameActionException {
        // Update location, as necessary.
        this.myLocation = rc.getLocation();
        
        if (this.rc.getSupplyLevel() == 0.0) {
            this.rc.setIndicatorDot(myLocation, 128, 128, 128);
        }
        
        SensorInfo sensorInfo = this.sense();
        if (NormalUnit.isDamaging(myType)) {
            this.attack(sensorInfo);
        }
        if (Clock.getRoundNum() == this.spawnedRound) {
            // Find any neighbor.
            for (Direction d: Bot.ALL_DIRECTIONS) {
                if (this.getStructureAt(myLocation.add(d)) != null) {
                    // Act as if we were spawned from the structure.
                    this.lastDirection = d.opposite();
                    break;
                }
            }
        }
        if (this.lastDirection != Direction.NONE) {
            MapLocation[] newLocations = NormalUnit.getNewSensableLocations(myLocation, this.lastDirection);
            for (MapLocation ml: newLocations) {
                this.recordTerrainTile(ml);
            }
        }
        
        this.performUnitSpecificActions(sensorInfo);
        
        this.lastDirection = Direction.NONE;
        if (this.rc.isCoreReady()) {
            this.dm.clear();

            Direction movementDirection = this.getCombatMovementDirection(sensorInfo);
            if (movementDirection == null) {
                movementDirection = this.getNonCombatMovementDirection(sensorInfo, this.dm);
            }
            
            if (movementDirection != null && movementDirection != Direction.NONE) {
                this.rc.move(movementDirection);
                this.lastDirection = movementDirection;
            }
        }
        
        
    }
    
    // The core is guaranteed to be ready if this is called.
    protected abstract Direction getNonCombatMovementDirection(SensorInfo sensorInfo, DirectionMap dm) throws GameActionException;
    
    // Returns the result of taking n steps from start to end.
    private static MapLocation stepsToward(MapLocation start, MapLocation end, int n) {
        MapLocation current = start;
        for (int i = 0; i < n; i++) {
            Direction d = current.directionTo(end);
            current = current.add(d);
        }
        return current;
    }
    
    private Direction getCombatMovementDirection(SensorInfo sensorInfo) throws GameActionException {
        for (Direction d : ALL_DIRECTIONS) {
            if (d != Direction.NONE && !this.rc.canMove(d)) {
                this.dm.addWeight(d, DirectionMap.ILLEGAL_MOVE);
            }
        }
        
        // Don't get too close to the enemy HQ.
        final int SPLASH_DAMAGE_TOWERS = 5;
        int extraStepsToEnemyHQ = 0;
        MapLocation attackTarget = this.com.getAttackTarget();
        
        if (this.rc.senseEnemyTowerLocations().length >= SPLASH_DAMAGE_TOWERS) {
            extraStepsToEnemyHQ = 1;
        }
        if (NormalUnit.stepsToward(myLocation, enemyHQLocation, 1 + extraStepsToEnemyHQ).distanceSquaredTo(enemyHQLocation) <= RobotType.HQ.attackRadiusSquared) {
            boolean shouldRespectHQ = !this.shouldIgnoreStructureDamage(attackTarget, enemyHQLocation);
            if (shouldRespectHQ) {
                for (Direction d : ALL_DIRECTIONS) {
                    MapLocation candidateLocation = myLocation.add(d);
                    int distanceToEnemyHQ = NormalUnit.stepsToward(candidateLocation, enemyHQLocation, extraStepsToEnemyHQ).distanceSquaredTo(enemyHQLocation);
                    if (distanceToEnemyHQ <= RobotType.HQ.attackRadiusSquared) {
                        this.dm.addWeight(d, -RobotType.HQ.attackPower / RobotType.HQ.attackDelay * (1.0 + 1.0 / distanceToEnemyHQ));
                    }
                }
            }
        }
        
        // Don't get too close to the enemy tower.
        MapLocation[] enemyTowers = this.rc.senseEnemyTowerLocations();
        for (MapLocation enemyTower: enemyTowers) {
            Direction directionToEnemyTower = myLocation.directionTo(enemyTower);
            if (myLocation.add(directionToEnemyTower).distanceSquaredTo(enemyTower) > RobotType.TOWER.attackRadiusSquared) {
                // Tower doesn't really matter here.
                continue;
            }
            if (this.shouldIgnoreStructureDamage(attackTarget, enemyTower)) {
                continue;
            }
            for (Direction d : ALL_DIRECTIONS) {
                MapLocation candidateLocation = myLocation.add(d);
            
                int distanceToEnemyTower = candidateLocation.distanceSquaredTo(enemyTower);
                if (distanceToEnemyTower <= RobotType.TOWER.attackRadiusSquared) {
                    this.dm.addWeight(d, -RobotType.TOWER.attackPower / RobotType.TOWER.attackDelay * (1.0 + 1.0 / distanceToEnemyTower));
                }
            }
        }
        
        boolean hasDPSAdvantage = NormalUnit.haveMoreDPS(sensorInfo);
        
        RobotInfo[] myInfos = sensorInfo.myInfos;
        RobotInfo[] enemyInfos = sensorInfo.enemyInfos;
        
        for (RobotInfo enemyInfo : enemyInfos) {
            MapLocation enemyLocation = enemyInfo.location;
            RobotType enemyType = enemyInfo.type;
            
            if (this.isPassiveUnit()) {
                // Special "run-away" micro for passive units.
                for (Direction d : ALL_DIRECTIONS_INCLUDING_NONE) {
                    MapLocation candidateLocation = this.myLocation.add(d);
                    int distance = candidateLocation.distanceSquaredTo(enemyLocation);
                    if (distance <= enemyType.attackRadiusSquared) {
                        this.dm.addWeight(d, -enemyType.attackPower / enemyType.attackDelay - 0.2 / (1.0 + distance));
                    }
                }
                continue;
            }
            
            boolean shouldBeAggressive = false;
            
            if (enemyType == RobotType.HQ || enemyType == RobotType.TOWER) {
                if (this.shouldIgnoreStructureDamage(attackTarget, enemyLocation)) {
                    // Let's go attack the tower.
                    shouldBeAggressive = false;
                } else {
                    // Ignore, because we've already considered towers and HQ.
                    continue;
                }
            } else if (enemyInfo.type == RobotType.MISSILE) {
                // Don't run headfirst into a missile.
                shouldBeAggressive = false;
            } else if (NormalUnit.isEnemyInCombat(enemyInfo, sensorInfo)) {
                shouldBeAggressive = true;
            }
            
            for (Direction d : ALL_DIRECTIONS_INCLUDING_NONE) {
                MapLocation candidateLocation = this.myLocation.add(d);
                int distance = candidateLocation.distanceSquaredTo(enemyLocation);
                if (hasDPSAdvantage) {
                    // Move toward the enemy, since we have a DPS advantage.
                    if (distance <= myType.attackRadiusSquared) {
                        this.dm.addWeight(d, 0.1 / (1.0 + distance));
                    }
                } else if (shouldBeAggressive) {
                    if (distance <= enemyType.attackRadiusSquared) {
                        this.dm.addWeight(d, -enemyType.attackPower / enemyType.attackDelay);
                    } else {
                        // Try to keep vision, but don't go crazy over it.
                        this.dm.addWeight(d, 0.1 / (1.0 + distance));
                    }
                } else {
                    double weight = 1.0;
                    double extraWeight;
                    boolean shouldMoveCloser = false;
                    
                    if (distance <= enemyType.attackRadiusSquared &&
                            myType.attackRadiusSquared < enemyType.attackRadiusSquared) {
                        shouldMoveCloser = true;
                    }
                    if (shouldMoveCloser) {
                        // Move closer, to allow room for other units to attack.
                        extraWeight = 0.2 / (1.0 + distance);
                    } else {
                        // Move away, to kite
                        extraWeight = -0.2 / (1.0 + distance);
                    }
                    this.dm.addWeight(d, weight + extraWeight);
                }
            }
        }
        
        if (this.dm.isMoveForced()) {
            Direction forcedDirection = this.dm.getBestDirection();
            if (forcedDirection != Direction.NONE) {
                if (!rc.canMove(forcedDirection)) {
                    System.out.println("??? " + forcedDirection.toString());
                }
                return forcedDirection;
            }
            return Direction.NONE;
        }
        return null;
    }
    
    public static boolean haveMoreDPS(SensorInfo info) {
        double ourDPS = 0.0;
        double enemyDPS = 0.0;
        
        for (RobotInfo ri: info.myInfos) {
            if (ri.type.isBuilding) {
                continue;
            }
            ourDPS += (ri.type.attackPower / ri.type.attackDelay);
        }
        for (RobotInfo ri: info.enemyInfos) {
            enemyDPS += (ri.type.attackPower / ri.type.attackDelay);
        }
        
        return ourDPS >= enemyDPS * 2.0;
    }
    
    public static boolean isDamaging(RobotType type) {
        switch (type) {
            case AEROSPACELAB:
            case BARRACKS:
            case COMPUTER:
            case HANDWASHSTATION:
            case HELIPAD:
            case MINERFACTORY:
            case SUPPLYDEPOT:
            case TANKFACTORY:
            case TECHNOLOGYINSTITUTE:
            case TRAININGFIELD:
                return false;
            case BASHER:
            case BEAVER:
            case COMMANDER:
            case DRONE:
            case HQ:
            case LAUNCHER:
            case MINER:
            case MISSILE:
            case SOLDIER:
            case TANK:
            case TOWER:
                return true;
            default:
                throw new AssertionError("Robot type not recognized: " + type.toString());
        }
    }

    private static boolean isEnemyInCombat(RobotInfo enemyInfo, SensorInfo sensorInfo) {
        if (!NormalUnit.isDamaging(enemyInfo.type)) {
            return false;
        }
        
        MapLocation enemyLocation = enemyInfo.location;
        int enemyRange = enemyInfo.type.attackRadiusSquared;
        
        for (RobotInfo ri: sensorInfo.myInfos) {
            if (ri.location.distanceSquaredTo(enemyLocation) <= enemyRange) {
                return false;
            }
        }
        return true;
    }
    
    private boolean shouldIgnoreStructureDamage(MapLocation attackTarget, MapLocation enemyLocation) throws GameActionException {
        int siegeIndex = this.siegeLocationToInt(enemyLocation);
        if (Clock.getRoundNum() > this.com.getSiegeTakedownRound(siegeIndex)) {
            return false;
        }
        
        if (enemyLocation.distanceSquaredTo(attackTarget) > NormalUnit.TOWER_CLUMP_THRESHOLD) {
            return false;
        }
        return true;
    }
    
    private static MapLocation[] getNewSensableLocations(MapLocation ml, Direction d) {
        switch (d) {
            case NORTH:
                return new MapLocation[] {
                    ml.add(-4, 2),
                    ml.add(-3, 3),
                    ml.add(-2, 4),
                    ml.add(-1, 4),
                    ml.add(0, 4),
                    ml.add(1, 4),
                    ml.add(2, 4),
                    ml.add(3, 3),
                    ml.add(4, 2),
                };
            case NORTH_EAST:
                return new MapLocation[] {
                    ml.add(-2, 4),
                    ml.add(-1, 4),
                    ml.add(0, 4),
                    ml.add(1, 4),
                    ml.add(2, 4),
                    ml.add(2, 3),
                    ml.add(3, 3),
                    ml.add(3, 2),
                    ml.add(4, 2),
                    ml.add(4, 1),
                    ml.add(4, 0),
                    ml.add(4, -1),
                    ml.add(4, -2),
                };
            case EAST:
                return new MapLocation[] {
                    ml.add(2, 4),
                    ml.add(3, 3),
                    ml.add(4, 2),
                    ml.add(4, 1),
                    ml.add(4, 0),
                    ml.add(4, -1),
                    ml.add(4, -2),
                    ml.add(3, -3),
                    ml.add(2, -4),
                };
            case SOUTH_EAST:
                return new MapLocation[] {
                    ml.add(4, 2),
                    ml.add(4, 1),
                    ml.add(4, 0),
                    ml.add(4, -1),
                    ml.add(4, -2),
                    ml.add(3, -2),
                    ml.add(3, -3),
                    ml.add(2, -3),
                    ml.add(2, -4),
                    ml.add(1, -4),
                    ml.add(0, -4),
                    ml.add(-1, -4),
                    ml.add(-2, -4),
                };
            case SOUTH:
                return new MapLocation[] {
                    ml.add(4, -2),
                    ml.add(3, -3),
                    ml.add(2, -4),
                    ml.add(1, -4),
                    ml.add(0, -4),
                    ml.add(-1, -4),
                    ml.add(-2, -4),
                    ml.add(-3, -3),
                    ml.add(-4, -2),
                };
            case SOUTH_WEST:
                return new MapLocation[] {
                    ml.add(2, -4),
                    ml.add(1, -4),
                    ml.add(0, -4),
                    ml.add(-1, -4),
                    ml.add(-2, -4),
                    ml.add(-2, -3),
                    ml.add(-3, -3),
                    ml.add(-3, -2),
                    ml.add(-4, -2),
                    ml.add(-4, -1),
                    ml.add(-4, 0),
                    ml.add(-4, 1),
                    ml.add(-4, 2),
                };
            case WEST:
                return new MapLocation[] {
                    ml.add(-2, 4),
                    ml.add(-3, 3),
                    ml.add(-4, 2),
                    ml.add(-4, 1),
                    ml.add(-4, 0),
                    ml.add(-4, -1),
                    ml.add(-4, -2),
                    ml.add(-3, -3),
                    ml.add(-2, -4),
                };
            case NORTH_WEST:
                return new MapLocation[] {
                    ml.add(-4, -2),
                    ml.add(-4, -1),
                    ml.add(-4, 0),
                    ml.add(-4, 1),
                    ml.add(-4, 2),
                    ml.add(-3, 2),
                    ml.add(-3, 3),
                    ml.add(-2, 3),
                    ml.add(-2, 4),
                    ml.add(-1, 4),
                    ml.add(0, 4),
                    ml.add(1, 4),
                    ml.add(2, 4),
                };
            default:
                return new MapLocation[]{};
        }
    }
    
    protected boolean hasPathToAttackTarget(MapLocation target) {
        Direction directionToTarget = myLocation.directionTo(target);
        Direction[] candidateDirections = new Direction[]{
            directionToTarget,
            directionToTarget.rotateLeft(),
            directionToTarget.rotateRight(),
        };
        for (Direction d: candidateDirections) {
            MapLocation current = myLocation.add(d);
            if (!this.rc.isPathable(myType, current)) {
                continue;
            }
            boolean success = true;
            while (current.distanceSquaredTo(target) > myType.attackRadiusSquared) {
                Direction nextDirection = current.directionTo(target);
                MapLocation next = current.add(nextDirection);
                if (!this.rc.isPathable(myType, next)) {
                    success = false;
                    break;
                }
                current = next;
            }
            if (success) {
                return true;
            }
        }
        return false;
    }
}
