package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class RobotPlayer {
    public static void run(RobotController rc) {
        switch (rc.getType()) {
            case HQ:
                (new HQ(rc)).run();
                break;
            case BEAVER:
                (new Beaver(rc)).run();
                break;
            case TOWER:
                (new Tower(rc)).run();
                break;
            case MINERFACTORY:
                (new MinerFactory(rc)).run();
                break;
            case MINER:
                (new Miner(rc)).run();
                break;
            case HELIPAD:
                (new Helipad(rc)).run();
                break;
            case DRONE:
                (new Drone(rc)).run();
                break;
            case AEROSPACELAB:
                (new AerospaceLab(rc)).run();
                break;
            case LAUNCHER:
                (new Launcher(rc)).run();
                break;
            case MISSILE:
                (new Missile(rc)).run();
                break;
            case BARRACKS:
                (new Barracks(rc)).run();
                break;
            case TANKFACTORY:
                (new TankFactory(rc)).run();
                break;
            case TANK:
                (new Tank(rc)).run();
                break;
            case SUPPLYDEPOT:
                (new SupplyDepot(rc)).run();
                break;
            default:
                System.err.println("???:" + rc.getType().toString());
                break;
        }
    }
}
