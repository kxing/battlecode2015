package team312_original;

import battlecode.common.*;

/**
 *
 * @author kxing
 */
public class Com {
    private final RobotController rc;
    private final MapLocation myHQLocation;
    // The smallest possible X and Y values on the map, given only the location of the HQ.
    private final int minX;
    private final int minY;
    
    // Proposed locations: channels 0-6, inclusive.
    private static final int PROPOSED_LOCATION_START = 0;
    
    // Map dimension information: channels 7-10, inclusive.
    private static final int MIN_X_CHANNEL = 7;
    private static final int MAX_X_CHANNEL = 8;
    private static final int MIN_Y_CHANNEL = 9;
    private static final int MAX_Y_CHANNEL = 10;
    
    // Commands for beavers: channel 11.
    private static final int BEAVER_CHANNEL = 11;
    
    // Commands for static structures: channels 12-18, inclusive.
    private static final int MINER_FACTORY_CHANNEL = 12;
    private static final int TECHNOLOGY_INSTITUTE_CHANNEL = 13;
    private static final int TRAINING_FIELD_CHANNEL = 14;
    private static final int BARRACKS_CHANNEL = 15;
    private static final int TANK_FACTORY_CHANNEL = 16;
    private static final int HELIPAD_CHANNEL = 17;
    private static final int AEROSPACE_LABL_CHANNEL = 18;
    
    // Commands for miners: channels 19-21;
    private static final int MINER_GOAL_CHANNEL = 19;
    private static final int MINER_IS_POLLING_CHANNEL = 20;
    // So that the HQ can count the number of miners.
    private static final int MINER_NUMBER_CHANNEL = 21;
    // Register good mining locations: channels 22-72.
    private static final int MINER_NUM_GOOD_LOCATIONS_CHANNEL = 22;
    private static final int MINER_GOOD_LOCATIONS_START = 23;
    public static final int MINER_MAX_NUM_GOOD_LOCATIONS = 73 - MINER_GOOD_LOCATIONS_START;
    
    // Commands for scouting: channel 73.
    private static final int SCOUTING_GOAL_CHANNEL = 73;
    
    // Command for building a supplier: channel 74.
    private static final int SUPPLIER_BUILD_CHANNEL = 74;
    
    // Commands for rallying units: channel 90-91.
    private static final int RALLY_DONE_CHANNEL = 90;
    private static final int RALLY_LOCATION_CHANNEL = 91;
    
    // Commands for attacking: channels 100.
    private static final int ATTACK_CHANNEL = 100;
    
    // Commands for sieging. Three channels for each of the seven objectives
    // (six towers and one HQ) -> 3 * 7 = 21 channels: channels 102-122.
    // The first one corresponds to the one for the HQ.
    private static final int SIEGE_START_CHANNEL = 102;
    private static final int SIEGE_LOCATION_CHANNEL_OFFSET = 0;
    private static final int SIEGE_TAKEDOWN_CHANNEL_OFFSET = 1;
    private static final int SIEGE_NET_DPS_CHANNEL_OFFSET = 2;
    private static final int SIEGE_CHANNELS_PER_TARGET = 3;
    
    // Defensive tower channels: channels 150-161.
    private static final int DEFENSE_TOWER_START_CHANNEL = 150;
    private static final int DEFENSE_TOWER_LOCATION_CHANNEL_OFFSET = 0;
    private static final int DEFENSE_TOWER_UNDER_ATTACK_CHANNEL_OFFSET = 1;
    private static final int DEFENSE_CHANNELS_PER_TOWER = 2;
    
    // Map information: channels 8415 - 65535.
    private static final int MAP_CHANNEL_START =
            GameConstants.BROADCAST_MAX_CHANNELS -
            (2 * GameConstants.MAP_MAX_HEIGHT - 1) * (2 * GameConstants.MAP_MAX_WIDTH - 1) + 1;
            
    
    public Com(RobotController rc) {
        this.rc = rc;
        this.myHQLocation = rc.senseHQLocation();
        this.minX = this.myHQLocation.x - GameConstants.MAP_MAX_WIDTH + 1;
        this.minY = this.myHQLocation.y - GameConstants.MAP_MAX_HEIGHT + 1;
    }
    
    // -------------------------------------------------------------------------
    // For encoding where to build all of the structures.
    // -------------------------------------------------------------------------
    
    public void setProposedStructureLocation(RobotType type, MapLocation ml) throws GameActionException {
        int channelData = this.mapLocationToInt(ml);
        rc.broadcast(PROPOSED_LOCATION_START + Com.structureTypeToInt(type), channelData);
    }
    
    public MapLocation getProposedStructureLocation(RobotType type) throws GameActionException {
        int channelData = rc.readBroadcast(PROPOSED_LOCATION_START + Com.structureTypeToInt(type));
        return this.intToMapLocation(channelData);
    }
    
    // -------------------------------------------------------------------------
    // For encoding the map dimensions.
    // -------------------------------------------------------------------------
    
    private static final int MAP_EXTREMA_OFFSET = Math.max(GameConstants.MAP_MAX_HEIGHT, GameConstants.MAP_MAX_WIDTH);
    
    private void debug_mapDimension(int channel) throws GameActionException {
        if (this.rc.readBroadcast(channel) == 0) {
            throw new AssertionError("map dimension not set yet");
        }
    }
    
    public void setMinX(int minX) throws GameActionException {
        this.rc.broadcast(MIN_X_CHANNEL, MAP_EXTREMA_OFFSET + minX - this.myHQLocation.x);
    }
    
    public int getMinX() throws GameActionException {
        debug_mapDimension(MIN_X_CHANNEL);
        return this.rc.readBroadcast(MIN_X_CHANNEL) + this.myHQLocation.x - MAP_EXTREMA_OFFSET;
    }
    
    public boolean isMinXSet() throws GameActionException {
        return (this.rc.readBroadcast(MIN_X_CHANNEL) != 0);
    }
    
    public void setMinY(int minY) throws GameActionException {
        this.rc.broadcast(MIN_Y_CHANNEL, MAP_EXTREMA_OFFSET + minY - this.myHQLocation.y);
    }
    
    public int getMinY() throws GameActionException {
        debug_mapDimension(MIN_Y_CHANNEL);
        return this.rc.readBroadcast(MIN_Y_CHANNEL) + this.myHQLocation.y - MAP_EXTREMA_OFFSET;
    }
    
    public boolean isMinYSet() throws GameActionException {
        return (this.rc.readBroadcast(MIN_Y_CHANNEL) != 0);
    }
    
    public void setMaxX(int maxX) throws GameActionException {
        this.rc.broadcast(MAX_X_CHANNEL, MAP_EXTREMA_OFFSET + maxX - this.myHQLocation.x);
    }
    
    public int getMaxX() throws GameActionException {
        return this.rc.readBroadcast(MAX_X_CHANNEL) + this.myHQLocation.x - MAP_EXTREMA_OFFSET;
    }
    
    public boolean isMaxXSet() throws GameActionException {
        return (this.rc.readBroadcast(MAX_X_CHANNEL) != 0);
    }
    
    public void setMaxY(int maxY) throws GameActionException {
        this.rc.broadcast(MAX_Y_CHANNEL, MAP_EXTREMA_OFFSET + maxY - this.myHQLocation.y);
    }
    
    public int getMaxY() throws GameActionException {
        return this.rc.readBroadcast(MAX_Y_CHANNEL) + this.myHQLocation.y - MAP_EXTREMA_OFFSET;
    }
    
    public boolean isMaxYSet() throws GameActionException {
        return (this.rc.readBroadcast(MAX_Y_CHANNEL) != 0);
    }
    
    // -------------------------------------------------------------------------
    // For encoding what the beaver should be doing.
    // -------------------------------------------------------------------------
    
    public static final int BEAVER_GOAL_GO_MINING = 0;
    public static final int BEAVER_GOAL_BUILD_MINER_FACTORY = 1;
    public static final int BEAVER_GOAL_BUILD_TECHNOLOGY_INSTITUTE = 2;
    public static final int BEAVER_GOAL_BUILD_TRAINING_FIELD = 3;
    public static final int BEAVER_GOAL_BUILD_BARRACKS = 4;
    public static final int BEAVER_GOAL_BUILD_TANK_FACTORY = 5;
    public static final int BEAVER_GOAL_BUILD_HELIPAD = 6;
    public static final int BEAVER_GOAL_BUILD_AEROSPACE_LAB = 7;
    public static final int BEAVER_GOAL_BUILD_SUPPLIER = 8;
    
    public int getBeaverGoal() throws GameActionException {
        return this.rc.readBroadcast(BEAVER_CHANNEL);
    }
    
    public void setBeaverGoal(int beaverGoal) throws GameActionException {
        this.rc.broadcast(BEAVER_CHANNEL, beaverGoal);
    }
    
    public MapLocation getSupplierLocation() throws GameActionException {
        int channelData = this.rc.readBroadcast(SUPPLIER_BUILD_CHANNEL);
        return this.intToMapLocation(channelData);
    }
    
    public void setSupplierLocation(MapLocation ml) throws GameActionException {
        int channelData = this.mapLocationToInt(ml);
        this.rc.broadcast(SUPPLIER_BUILD_CHANNEL, channelData);
    }
    
    // -------------------------------------------------------------------------
    // For encoding what the miners should be doing.
    // -------------------------------------------------------------------------
    
    public MapLocation getMinerGoal() throws GameActionException {
        int channelData = this.rc.readBroadcast(MINER_GOAL_CHANNEL);
        return this.intToMapLocation(channelData);
    }
    
    public void setMinerGoal(MapLocation ml) throws GameActionException {
        int channelData = this.mapLocationToInt(ml);
        this.rc.broadcast(MINER_GOAL_CHANNEL, channelData);
    }
    
    public boolean isMinerPolling() throws GameActionException {
        return this.rc.readBroadcast(MINER_IS_POLLING_CHANNEL) != 0;
    }
    
    public void setMinerPolling(boolean polling) throws GameActionException {
        this.rc.broadcast(MINER_IS_POLLING_CHANNEL, polling ? 1 : 0);
    }
    
    public void clearMinerCount() throws GameActionException {
        this.rc.broadcast(MINER_NUMBER_CHANNEL, 0);
    }
    
    public int getNumberOfMiners() throws GameActionException {
        return this.rc.readBroadcast(MINER_NUMBER_CHANNEL);
    }
    
    public void registerMiner() throws GameActionException {
        int numMiners = this.getNumberOfMiners();
        this.rc.broadcast(MINER_NUMBER_CHANNEL, numMiners + 1);
    }
    
    public int getNumberOfGoodMinerLocations() throws GameActionException {
        return this.rc.readBroadcast(MINER_NUM_GOOD_LOCATIONS_CHANNEL);
    }
    
    public void setNumberOfGoodMinerLocations(int numGoodLocations) throws GameActionException {
        this.rc.broadcast(MINER_NUM_GOOD_LOCATIONS_CHANNEL, numGoodLocations);
    }
    
    private void debug_minerLocationIndex(int index) {
        if (index < 0 || index >= MINER_MAX_NUM_GOOD_LOCATIONS) {
            throw new AssertionError("Invalid index: " + index);
        }
    }
    
    public MapLocation getGoodMinerLocation(int index) throws GameActionException {
        debug_minerLocationIndex(index);
        int channelData = this.rc.readBroadcast(MINER_GOOD_LOCATIONS_START + index);
        return this.intToMapLocation(channelData);
    }
    
    public void setGoodMinerLocation(int index, MapLocation ml) throws GameActionException {
        debug_minerLocationIndex(index);
        int channelData = this.mapLocationToInt(ml);
        this.rc.broadcast(MINER_GOOD_LOCATIONS_START + index, channelData);
    }
    
    // -------------------------------------------------------------------------
    // For scouting
    // -------------------------------------------------------------------------
    
    public MapLocation getScoutingGoal() throws GameActionException {
        int channelData = this.rc.readBroadcast(SCOUTING_GOAL_CHANNEL);
        return this.intToMapLocation(channelData);
    }
    
    public void setScoutingGoal(MapLocation ml) throws GameActionException {
        int channelData = this.mapLocationToInt(ml);
        this.rc.broadcast(SCOUTING_GOAL_CHANNEL, channelData);
    }
    
    // -------------------------------------------------------------------------
    // For determining when to stop rallying.
    // -------------------------------------------------------------------------
    
    public int getRallyDoneRound() throws GameActionException {
        return this.rc.readBroadcast(RALLY_DONE_CHANNEL);
    }
    
    public void setRallyDoneRound(int rallyDoneRound) throws GameActionException {
        this.rc.broadcast(RALLY_DONE_CHANNEL, rallyDoneRound);
    }
    
    public MapLocation getRallyLocation() throws GameActionException {
        int channelData = this.rc.readBroadcast(RALLY_LOCATION_CHANNEL);
        return this.intToMapLocation(channelData);
    }
    
    public void setRallyLocation(MapLocation ml) throws GameActionException {
        int channelData = this.mapLocationToInt(ml);
        this.rc.broadcast(RALLY_LOCATION_CHANNEL, channelData);
    }
        
    // -------------------------------------------------------------------------
    // For what each of the structures should be spawning.
    // -------------------------------------------------------------------------
    
    public void setStructureGoal(RobotType structureType, RobotType unitType) throws GameActionException {
        this.rc.broadcast(Com.structureTypeToChannel(structureType), Com.unitTypeToInt(unitType));
    }
    
    public RobotType getStructureGoal(RobotType structureType) throws GameActionException {
        int channelData = this.rc.readBroadcast(Com.structureTypeToChannel(structureType));
        return Com.intToUnitType(channelData);
    }
    
    // -------------------------------------------------------------------------
    // For keeping track of attacking.
    // -------------------------------------------------------------------------
    public void setAttackTarget(MapLocation ml) throws GameActionException {
        this.rc.broadcast(ATTACK_CHANNEL, this.mapLocationToInt(ml));
    }
    
    public MapLocation getAttackTarget() throws GameActionException {
        int channelData = this.rc.readBroadcast(ATTACK_CHANNEL);
        return this.intToMapLocation(channelData);
    }
    
    // -------------------------------------------------------------------------
    // For attacking and sieging.
    // -------------------------------------------------------------------------
    
    private int getSiegeLocationChannel(int index) {
        return SIEGE_START_CHANNEL + SIEGE_CHANNELS_PER_TARGET * index + SIEGE_LOCATION_CHANNEL_OFFSET;
    }
    
    public MapLocation getSiegeLocation(int index) throws GameActionException {
        int channelData = this.rc.readBroadcast(this.getSiegeLocationChannel(index));
        return this.intToMapLocation(channelData);
    }
    
    public void setSiegeLocation(int index, MapLocation ml) throws GameActionException {
        int channelData = this.mapLocationToInt(ml);
        this.rc.broadcast(this.getSiegeLocationChannel(index), channelData);
    }
    
    private int getSiegeTakedownRoundChannel(int index) {
        return SIEGE_START_CHANNEL + SIEGE_CHANNELS_PER_TARGET * index + SIEGE_TAKEDOWN_CHANNEL_OFFSET;
    }
    
    public int getSiegeTakedownRound(int index) throws GameActionException {
        return this.rc.readBroadcast(this.getSiegeTakedownRoundChannel(index));
    }
    
    public void setSiegeTakedownRound(int index, int takedownRound) throws GameActionException {
        this.rc.broadcast(this.getSiegeTakedownRoundChannel(index), takedownRound);
    }
    
    private int getSiegeNetDPSChannel(int index) {
        return SIEGE_START_CHANNEL + SIEGE_CHANNELS_PER_TARGET * index + SIEGE_NET_DPS_CHANNEL_OFFSET;
    }
    
    public int getSiegeNetDPS(int index) throws GameActionException {
        return this.rc.readBroadcast(this.getSiegeNetDPSChannel(index));
    }
    
    public void resetSiegeNetDPS(int index) throws GameActionException {
        if (this.getSiegeNetDPS(index) != 0) {
            this.rc.broadcast(this.getSiegeNetDPSChannel(index), 0);
        }
    }
    
    public void incrementSiegeNetDPS(int index, int dps) throws GameActionException {
        int currentDPS = this.getSiegeNetDPS(index);
        this.rc.broadcast(this.getSiegeNetDPSChannel(index), currentDPS + dps);
    }
    
    // -------------------------------------------------------------------------
    // For defending towers.
    // -------------------------------------------------------------------------
    // Defensive tower channels: channels 150-161.
//    private static final int DEFENSE_TOWER_START_CHANNEL = 150;
//    private static final int DEFENSE_TOWER_LOCATION_CHANNEL_OFFSET = 0;
//    private static final int DEFENSE_TOWER_UNDER_ATTACK_CHANNEL_OFFSET = 1;
    
    private int getDefenseTowerLocationChannel(int index) {
        return DEFENSE_TOWER_START_CHANNEL + DEFENSE_CHANNELS_PER_TOWER * index +
                DEFENSE_TOWER_LOCATION_CHANNEL_OFFSET;
    }
    
    private int getDefenseTowerUnderAttackChannel(int index) {
        return DEFENSE_TOWER_START_CHANNEL + DEFENSE_CHANNELS_PER_TOWER * index +
                DEFENSE_TOWER_UNDER_ATTACK_CHANNEL_OFFSET;
    }
    
    public MapLocation getDefenseTowerLocation(int index) throws GameActionException {
        int channelData = this.rc.readBroadcast(this.getDefenseTowerLocationChannel(index));
        return this.intToMapLocation(channelData);
    }
    
    public void setDefenseTowerLocation(int index, MapLocation ml) throws GameActionException {
        int channelData = this.mapLocationToInt(ml);
        this.rc.broadcast(this.getDefenseTowerLocationChannel(index), channelData);
    }
    
    public int getDefenseTowerUnderAttack(int index) throws GameActionException {
        return this.rc.readBroadcast(this.getDefenseTowerUnderAttackChannel(index));
    }
    
    public void setDefenseTowerUnderAttack(int index, int roundNumber) throws GameActionException {
        this.rc.broadcast(this.getDefenseTowerUnderAttackChannel(index), roundNumber);
    }
    
    // -------------------------------------------------------------------------
    // For encoding the actual map, along with the ore counts.
    // -------------------------------------------------------------------------
    
    // The default value (0) always corresponds to null, which will be convenient
    // because the message array should be initialized to 0 anyways.
    private static final int NO_VALUE = 0;
    
    // Constants for encoding the map state.
    private static final int OFF_MAP_INT = 1;
    private static final int VOID_INT = 2;
    private static final int NORMAL_OFFSET = 3;
    
    public TerrainTile getTerrainTile(MapLocation ml) throws GameActionException {
        int channelData = this.rc.readBroadcast(this.mapChannel(ml));
        return Com.intToTerrainTile(channelData);
    }
    
    private void debug_setTerrainTile(MapLocation ml, TerrainTile tile) throws GameActionException {
        if (tile == TerrainTile.UNKNOWN) {
            throw new AssertionError("Why set the tile as UNKNOWN?");
        }
        TerrainTile currentTile = this.getTerrainTile(ml);
        if (currentTile != TerrainTile.UNKNOWN && currentTile != tile) {
            throw new AssertionError("Logged terrain tiles don't match for " + ml.toString());
        }
    }
    
    public void setTerrainTile(MapLocation ml, TerrainTile tile) throws GameActionException {
        debug_setTerrainTile(ml, tile);
        this.rc.broadcast(this.mapChannel(ml), Com.terrainTileToInt(tile));
    }
    
    public void debug_printMap() throws GameActionException {
        StringBuilder map = new StringBuilder().append('\n');
        int minX = -GameConstants.MAP_MAX_WIDTH + 1;
        if (this.isMinXSet()) {
            minX = this.getMinX() - this.myHQLocation.x;
        }
        int minY = -GameConstants.MAP_MAX_HEIGHT + 1;
        if (this.isMinYSet()) {
            minY = this.getMinY() - this.myHQLocation.y;
        }
        int maxX = GameConstants.MAP_MAX_WIDTH - 1;
        if (this.isMaxXSet()) {
            maxX = this.getMaxX() - this.myHQLocation.x;
        }
        int maxY = GameConstants.MAP_MAX_HEIGHT - 1;
        if (this.isMaxYSet()) {
            maxY = this.getMaxY() - this.myHQLocation.y;
        }
        for (int dy = minY; dy <= maxY; dy++) {
            for (int dx = minX; dx <= maxX; dx++) {
                MapLocation ml = new MapLocation(this.myHQLocation.x + dx, this.myHQLocation.y + dy);
                TerrainTile tile = this.getTerrainTile(ml);
                switch (tile) {
                    case NORMAL:
                        map.append(' ');
                        break;
                    case OFF_MAP:
                        map.append('O');
                        break;
                    case VOID:
                        map.append('X');
                        break;
                    case UNKNOWN:
                        map.append('?');
                        break;
                    default:
                        break;
                }
            }
            map.append('\n');
        }
        System.out.println(map);
    }
    
    private void debug_setOreAmount(MapLocation ml) throws GameActionException {
        if (this.getTerrainTile(ml) != TerrainTile.NORMAL) {
            throw new AssertionError("Called setOreAmount() on a tile with no ore.");
        }
    }
    
    public void setOreAmount(MapLocation ml, double amount) throws GameActionException {
        debug_setOreAmount(ml);
        int channelData = NORMAL_OFFSET + (int)(amount);
        this.rc.broadcast(this.mapChannel(ml), channelData);
    }
    
    private void debug_getOreAmount(MapLocation ml) throws GameActionException {
        if (this.getTerrainTile(ml) != TerrainTile.NORMAL) {
            throw new AssertionError("Called getOreAmount() on a tile with no ore.");
        }
    }
    
    public int getOreAmount(MapLocation ml) throws GameActionException {
        debug_getOreAmount(ml);
        int channelData = this.rc.readBroadcast(this.mapChannel(ml));
        return channelData - NORMAL_OFFSET;
    }
    
    // -------------------------------------------------------------------------
    // Here is where the actual encoding is done.
    // -------------------------------------------------------------------------
    
    // For encoding/decoding MapLocations
    private static final int X_BASE = 2 * GameConstants.MAP_MAX_HEIGHT;
    
    private int mapLocationToInt(MapLocation ml) {
        if (ml == null) {
            return Com.NO_VALUE;
        }
        return (ml.x - this.minX) * Com.X_BASE + (ml.y - this.minY) + 1;
    }
    
    private MapLocation intToMapLocation(int encodedInt) {
        if (encodedInt == Com.NO_VALUE) {
            return null;
        }
        int x = (encodedInt - 1) / Com.X_BASE + this.minX;
        int y = (encodedInt - 1) % Com.X_BASE + this.minY;
        
        return new MapLocation(x, y);
    }
    
    // For encoding/decoding TerrainTiles
    private static int terrainTileToInt(TerrainTile tile) {
        switch (tile) {
            case UNKNOWN:
                return NO_VALUE;
            case OFF_MAP:
                return OFF_MAP_INT;
            case VOID:
                return VOID_INT;
            default:
                return NORMAL_OFFSET;
        }
    }
    
    private static TerrainTile intToTerrainTile(int encodedInt) {
        switch (encodedInt) {
            case NO_VALUE:
                return TerrainTile.UNKNOWN;
            case OFF_MAP_INT:
                return TerrainTile.OFF_MAP;
            case VOID_INT:
                return TerrainTile.VOID;
            default:
                return TerrainTile.NORMAL;
        }
    }
    
    // For computing offsets, etc.
    
    private static int structureTypeToInt(RobotType type) {
        switch (type) {
            case MINERFACTORY:
                return 0;
            case TECHNOLOGYINSTITUTE:
                return 1;
            case TRAININGFIELD:
                return 2;
            case BARRACKS:
                return 3;
            case TANKFACTORY:
                return 4;
            case HELIPAD:
                return 5;
            case AEROSPACELAB:
                return 6;
            default:
                throw new AssertionError("Not a unit-building structure that needs to be built.");
        }
    }
    
    // For converting map locations to channel numbers.
    private int mapChannel(MapLocation ml) {
        int offsetX = ml.x - this.minX;
        int offsetY = ml.y - this.minY;
        
        return MAP_CHANNEL_START + offsetX * (2 * GameConstants.MAP_MAX_HEIGHT - 1) + offsetY;
    }
    
    // For converting beaver goals to robot types to build.
    public static RobotType beaverGoalToRobotType(int beaverGoal) {
        switch (beaverGoal) {
            case 1:
                return RobotType.MINERFACTORY;
            case 2:
                return RobotType.TECHNOLOGYINSTITUTE;
            case 3:
                return RobotType.TRAININGFIELD;
            case 4:
                return RobotType.BARRACKS;
            case 5:
                return RobotType.TANKFACTORY;
            case 6:
                return RobotType.HELIPAD;
            case 7:
                return RobotType.AEROSPACELAB;
            default:
                throw new AssertionError("beaver goal unexpected: " + beaverGoal);
        }
    }
    
    // For converting between unit types to build and ints for encoding.
    private static final RobotType[] UNIT_TYPES = new RobotType[] {
        null,
        RobotType.MINER,
        RobotType.COMPUTER,
        RobotType.SOLDIER,
        RobotType.BASHER,
        RobotType.DRONE,
        RobotType.TANK,
        RobotType.COMMANDER,
        RobotType.LAUNCHER,
    };
    
    public static int unitTypeToInt(RobotType type) {
        if (type == null) {
            return 0;
        }
        switch (type) {
            case MINER:
                return 1;
            case COMPUTER:
                return 2;
            case SOLDIER:
                return 3;
            case BASHER:
                return 4;
            case DRONE:
                return 5;
            case TANK:
                return 6;
            case COMMANDER:
                return 7;
            case LAUNCHER:
                return 8;
            default:
                throw new AssertionError("Unit type not spawnable from a structure: " + type.toString());
        }
    }
    
    private static RobotType intToUnitType(int i) {
        return Com.UNIT_TYPES[i];
    }
    
    // For telling structures what to build.
    
    private static int structureTypeToChannel(RobotType type) {
        switch (type) {
            case MINERFACTORY:
                return MINER_FACTORY_CHANNEL;
            case TECHNOLOGYINSTITUTE:
                return TECHNOLOGY_INSTITUTE_CHANNEL;
            case TRAININGFIELD:
                return TRAINING_FIELD_CHANNEL;
            case BARRACKS:
                return BARRACKS_CHANNEL;
            case TANKFACTORY:
                return TANK_FACTORY_CHANNEL;
            case HELIPAD:
                return HELIPAD_CHANNEL;
            case AEROSPACELAB:
                return AEROSPACE_LABL_CHANNEL;
            default:
                throw new AssertionError("Structure type not recognized: " + type.toString());
        }
    }
}
